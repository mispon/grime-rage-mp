import Browser from '../Browser'
import { TOGGLE_ADMIN_WINDOW, CHAR_SHOW_CREATOR } from '../../../../global/constants/ServerEvents'
import { F6 } from '../../constants/Keys'

let isOpen = false

mp.keys.bind(F6, true, () => {
    isOpen = !isOpen
    Browser.trigger(TOGGLE_ADMIN_WINDOW, isOpen)
    Browser.onWindowsToggle(isOpen)
})