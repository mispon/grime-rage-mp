import { SHOW_SIDEBAR, HIDE_SIDEBAR } from '../../../../global/constants/ServerEvents'
import Browser from '../Browser'

/*
** Управление состоянием боковой панели
*/
mp.events.add({
    [SHOW_SIDEBAR]: showSidebar,
    [HIDE_SIDEBAR]: hideSidebar,
})

// Открывает боковую панель
function showSidebar(sidebarEvent, data) {
    Browser.triggerJson(sidebarEvent, data)
    Browser.onWindowsToggle(true)
}

// Закрывает боковую панель
function hideSidebar() {
    Browser.onWindowsToggle(false)
}