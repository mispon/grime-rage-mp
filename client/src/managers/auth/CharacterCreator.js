import * as Events from '../../../../global/constants/ServerEvents'
import Browser from '../../browser/Browser'
import CameraManager from '../CameraManager'

/*
** Клиентская логика создания персонажа
*/

const player = mp.players.local
const defaultHeading = -185.0
const cameraPos = new mp.Vector3(402.87, -997.55, -98.5)
const cameraLookAt = new mp.Vector3(402.87, -996.4108, -98.5)

mp.events.add({
    [Events.CHAR_SHOW_CREATOR]: showCreator,
    [Events.CHAR_HIDE_CREATOR]: hideCreator,
    [Events.CHAR_GENDER_CHANGE]: genderChange,
    [Events.CHAR_ROTATE]: headingChange,
    [Events.CHAR_PREVIEW]: previewChanges,    
})

// Показывает окно создания персонажа
function showCreator() {
    Browser.trigger(Events.CHAR_SHOW_CREATOR)
    Browser.onWindowsToggle(true)
    CameraManager.createNew(cameraPos, new mp.Vector3(0, 0, 0), cameraLookAt, 45)
    toggleEnvironment(false)
}

// Скрывает окно создания
function hideCreator() {
    toggleEnvironment(true)
    Browser.onWindowsToggle(false)
    Browser.trigger(Events.CHAR_HIDE_CREATOR)
}

// Переключает состояние различных настроект игрока и интерфейса
export const toggleEnvironment = isActive => {
    mp.gui.chat.show(isActive)
    mp.game.ui.displayRadar(isActive)
    mp.game.ui.displayHud(isActive)
    player.clearTasksImmediately()
    player.freezePosition(!isActive)
    mp.game.cam.renderScriptCams(!isActive, false, 0, true, false)
}

// Обработчик смены пола
function genderChange(type) {
    player.model = mp.game.joaat(type === 0 ? 'mp_m_freemode_01' : 'mp_f_freemode_01')
    player.setHeading(defaultHeading)
}

// Обработчик поворота игрока
function headingChange(value) {
    let heading = defaultHeading - value
    player.setHeading(heading)
}

// Показывает пользовательские изменения
function previewChanges(charData) {
    const char = JSON.parse(charData)
    setParents(char)
    setAppearance(char)
    setFeatures(char.features)
}

// Устанавливает данные родителей
function setParents(char) {
    player.setHeadBlendData(
        char.genetics.mother, char.genetics.father, 0,
        char.genetics.mother, char.genetics.father, 0,
        char.genetics.similarity, char.genetics.skinSimilarity, 0.0,
        false
    )
}

// Устанавливает особенности внешности
function setAppearance(char) {
    // Особенности внешности
    setHeadOverlay(1, char.hair.beard, char.hair.beardColor)
    setHeadOverlay(2, char.face.eyebrows)
    setHeadOverlay(3, char.face.ageing)
    setHeadOverlay(4, char.face.makeup)
    setHeadOverlay(5, char.face.blush, char.face.blushColor)
    setHeadOverlay(6, char.face.complexion)
    setHeadOverlay(8, char.face.lipstick, char.face.lipstickColor)
    setHeadOverlay(9, char.face.freckles)
    player.setEyeColor(char.face.eyeColor)

    // Прическа и цвет
    player.setComponentVariation(2, char.hair.model, 0, 2)
    player.setHairColor(char.hair.color, char.hair.highlightColor)    
}

// Устанавливает особенности внешности
function setHeadOverlay(index, overlayId, color = 0) {
    if (overlayId === -1) overlayId = 255
    player.setHeadOverlay(index, overlayId, 1.0, color, 0)
}

// Устанавливает детали внешности
function setFeatures(features) {
    features.forEach((value, index) => {
        player.setFaceFeature(index, value)
    })
}