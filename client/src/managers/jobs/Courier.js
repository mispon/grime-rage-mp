import * as Events from '../../../../global/constants/ServerEvents'
import { LANG, COURIER_ON_TARGET } from '../../../../global/constants/PlayerVariables'
import { E } from '../../constants/Keys'
import Misc from '../../utils/Misc'
import { locale } from '../../../../global/Locale'

/*
** Клиентская логика работы курьером
*/

let blip
let isOrderCompleted

const player = mp.players.local

mp.events.add({
    [Events.COURIER_NEXT]: showTarget,
    [Events.COURIER_HIDE]: hideTarget
})

mp.keys.bind(E, true, () => {
    if (player.getVariable(COURIER_ON_TARGET) && !isOrderCompleted) {
        isOrderCompleted = true
        mp.events.callRemote(Events.COMPLETE_DELIVERY)
    }
})

// Показывает точку доставки
function showTarget(position) {
    isOrderCompleted = false
    mp.game.ui.setNewWaypoint(position.x, position.y)
    if (blip === undefined)
        blip = mp.blips.new(1, position, {color: 3, name: locale('courier', 'target', player.getVariable(LANG))})
    else
        blip.setCoords(position)
}

// Скрывает точку доставки
function hideTarget() {
    Misc.removeEntity(blip)
    mp.game.ui.setNewWaypoint(player.position.x, player.position.y)
}