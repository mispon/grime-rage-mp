import * as Events from '../../../../global/constants/ServerEvents'
import { LANG, IS_PILOT } from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import { H } from '../../constants/Keys'
import Browser from '../../browser/Browser'
import Misc from '../../utils/Misc'
import { locale } from '../../../../global/Locale'

/*
** Клиентская логика работы пилотом
*/

let marker
let blip
let shape
let isOrderComplete

const player = mp.players.local

mp.events.add({
    [Events.PILOT_SHOW_TARGET]: showTarget,
    [Events.PILOT_HIDE_TARGET]: hideTarget
})

mp.keys.bind(H, true, () => {
    if (player.getVariable(IS_PILOT) && player.onTarget && !isOrderComplete) {
        isOrderComplete = true
        hideTarget()
        mp.events.callRemote(Events.PILOT_COMPLETE_ORDER)
    }
})

// Показывает точку доставки на карте
function showTarget(position) {
    isOrderComplete = false
    marker = mp.markers.new(1, position, 5, {color: [16, 125, 161, 150]})
    blip = mp.blips.new(479, position, {color: 69, name: getLocale('trucker', 'target')})
    createShape(position)
}

// Создает колшейп точки доставки
function createShape(position) {
    shape = mp.colshapes.newCircle(position.x, position.y, 5.0)
    shape.onEnter = () => {
        player.onTarget = true
        Browser.trigger(Events.SHOW_HINT, '', getLocale('pilot', 'toComplete'), 5000)
    }
    shape.onExit = () => player.onTarget = null
}

// Скрывает точку доставки с карты
function hideTarget() {
    Misc.removeEntity(blip)
    Misc.removeEntity(marker)
    Misc.removeEntity(shape)
    blip = marker = shape = null
    mp.game.ui.setNewWaypoint(player.position.x, player.position.y)
}

// Возвращает локализированную текстовку
function getLocale(key, value) {
    return locale(key, value, player.getVariable(LANG))
}