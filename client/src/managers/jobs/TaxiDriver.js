import * as Events from '../../../../global/constants/ServerEvents'
import { LANG } from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import Browser from '../../browser/Browser'
import Misc from '../../utils/Misc'
import { locale } from '../../../../global/Locale'

/*
** Клиентская логика работы таксистом
*/

let taxiClient
let marker
let blip
let speedCheckerId

const player = mp.players.local

mp.events.add({
    [Events.TAXI_START_ORDER]: rawData => setTimeout(() => startOrder(rawData), getSearchTimeout()),
    [Events.TAXI_REMOVE_ORDER]: removeOrder,
})

// Обработчик начала заказа
function startOrder(rawData) {    
    const data = JSON.parse(rawData)
    Browser.trigger(Events.TAXI_HAS_ORDER, data.client.isHurry, data.orderCost)
    const beginShape = createOrderPoint(data.begin, arriveToClient)
    beginShape.data = data
    createTaxiClient(data)
    setMarkers(data.begin, true)    
}

// Создает клиента такси
function createTaxiClient(data) {
    const model = mp.game.joaat(data.client.model)
    if (!player.vehicle.isSeatFree(2)) {
        taxiClient.taskLeaveAnyVehicle(0, 0)
        Misc.removeEntity(taxiClient)
    }
    taxiClient = mp.peds.new(model, data.client.position, data.client.heading)
}

// Обработчик прибытия к клиенту
function arriveToClient() {
    if (!isCorrect()) return 
    setTimeout(() => warpClientInCar(this.data), 2000)
    createOrderPoint(this.data.end, arriveToTarget)
    setMarkers(this.data.end, false)
    Misc.removeEntity(this)
}

// Помещает клиента в машину
function warpClientInCar(data) {
    if (!player.vehicle) return
    taxiClient.taskWarpIntoVehicle(player.vehicle.handle, 2)
    mp.gui.chat.push(getMessage('taxiDriver', data.client.isHurry ? 'hurryingClientChat' : 'calmClientChat'))
    if (data.client.isHurry)
        Browser.trigger(Events.SET_TIMER, data.orderTime, Events.TAXI_TIME_IS_OUT, getMessage('basic', 'tip'))
    else
        speedCheckerId = setInterval(checkSpeed, 5000)
}

// Обработчик доставки клиента на место
function arriveToTarget() {
    if (!isCorrect()) return
    Browser.trigger(Events.RESET_TIMER)
    setTimeout(() => {
        taxiClient.taskLeaveAnyVehicle(0, 0)
        setTimeout(() => Misc.removeEntity(taxiClient), 1500)
    }, 2000)
    finishOrder()   
    Misc.removeEntity(this)
}

// Обработчик завершения заказа
function finishOrder() {
    clearInterval(speedCheckerId)
    clearMarkers()
    mp.events.callRemote(Events.TAXI_COMPLETE_ORDER)
}

// Показывает следующую точку заказа на карте
function setMarkers(position, isBegin) {
    clearMarkers()
    const blipType = isBegin ? 280 : 56
    mp.game.ui.setNewWaypoint(position.x, position.y)
    marker = mp.markers.new(1, position, 2, {color: [240, 150, 15, 150]})
    blip = mp.blips.new(blipType, position, {color: 47, name: getMessage('taxiDriver', 'order')})
}

// Удаляет активный заказ
function removeOrder() {
    clearMarkers()
    Misc.removeEntity(taxiClient)
}

// Проверяет скорость
function checkSpeed() {
    if (!player.vehicle) return
    let speed = player.vehicle.getSpeed() * 3.6
    if (speed > 80) {
        mp.events.callRemote(Events.TAXI_OVER_SPEED)
    }
}

// Создает точку выполнения заказа
function createOrderPoint(position, callback) {
    const shape = mp.colshapes.newCircle(position.x, position.y, 2.0)
    shape.onEnter = callback
    return shape
}

// Очищает карту от рабочих маркеров
function clearMarkers() {
    Misc.removeEntity(blip)
    Misc.removeEntity(marker)
    blip = marker = null    
    mp.game.ui.setNewWaypoint(player.position.x, player.position.y)
}

// Проверяет корректность игрока для активации точки
function isCorrect() {
    const vehicle = player.vehicle
    return vehicle && vehicle.getVariable(GENERAL_VEHICLE_KEY) === 'Taxi'
}

// Имитация поиска нового клиента
function getSearchTimeout() {
    return Math.floor(Math.random() * 30000) + 10000
} 

// Возвращает локализированное сообщение
function getMessage(key, value) {
    return locale(key, value, player.getVariable(LANG))
}