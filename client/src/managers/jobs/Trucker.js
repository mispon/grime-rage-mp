import * as Events from '../../../../global/constants/ServerEvents'
import { LANG } from '../../../../global/constants/PlayerVariables'
import Browser from '../../browser/Browser'
import Misc from '../../utils/Misc'
import { locale } from '../../../../global/Locale'

/*
** Клиентская логика дальнобойщиков
*/

let blip
let marker
let shape

const player = mp.players.local

mp.events.add({
    [Events.TRUCKER_SHOW_TARGET]: showTarget,
    [Events.TRUCKER_HIDE_TARGET]: hideTarget
})

// Показывает точку доставки на карте
function showTarget(position) {
    marker = mp.markers.new(1, position, 3, {color: [16, 125, 161, 150]})
    blip = mp.blips.new(479, position, {color: 69, name: getLocale('target')})
    mp.game.ui.setNewWaypoint(position.x, position.y)
    createShape(position)
}

// Создает колшейп точки доставки
function createShape(position) {
    shape = mp.colshapes.newCircle(position.x, position.y, 3.0)
    shape.onEnter = () => {
        mp.events.callRemote(Events.TRUCKER_ON_TARGET, true)
        Browser.trigger(Events.SHOW_HINT, '', getLocale('toComplete'), 5000)
    }
    shape.onExit = () => mp.events.callRemote(Events.TRUCKER_ON_TARGET, false)
}

// Скрывает точку доставки с карты
function hideTarget() {
    Misc.removeEntity(blip)
    Misc.removeEntity(marker)
    Misc.removeEntity(shape)
    blip = marker = shape = null
    mp.game.ui.setNewWaypoint(player.position.x, player.position.y)
}

// Возвращает локализированную текстовку
function getLocale(value) {
    return locale('trucker', value, player.getVariable(LANG))
}