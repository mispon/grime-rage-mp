import * as Events from '../../../../global/constants/ServerEvents'
import CameraManager from '../CameraManager'
import Browser from '../../browser/Browser'
import { toggleEnvironment } from '../auth/CharacterCreator'

const props = [0, 1, 2, 6, 7]

const player = mp.players.local
let defaultHeading
let cameraPositions
let dressingCamera

/*
** Клиентская логика магазина одежды
*/
mp.events.add({
    [Events.CS_ENTER_DR]: onEnterDressingRoom,
    [Events.CS_LEAVE_DR]: onLeaveDressingRoom,
    [Events.CS_PREVIEW_GOOD]: previewGood,
    [Events.CS_MOVE_CAMERA]: moveCamera,
    [Events.CS_ROTATE]: headingChange,
    [Events.CS_RESET_CLOTHES]: resetClothes
})

// Обработчик входа в примерочную
function onEnterDressingRoom(cameraData, info, heading) {
    defaultHeading = heading
    cameraPositions = JSON.parse(cameraData)
    dressingCamera = CameraManager.createNew(
        cameraPositions.hatsPosition, cameraPositions.rotation,
        new mp.Vector3(0, 0, 0), 45, 'DressingRoomCamera'
    )
    toggleEnvironment(false)
    Browser.trigger(Events.HUD_TOGGLE)
    Browser.triggerJson(Events.CS_ENTER_DR, info)
    Browser.onWindowsToggle(true)
}

// Обработчик выхода из примерочной
function onLeaveDressingRoom() {
    dressingCamera = null
    toggleEnvironment(true)    
    Browser.onWindowsToggle(false)
    Browser.trigger(Events.HUD_TOGGLE)
    resetClothes()
    mp.events.callRemote(Events.CS_LEAVE_DR)
}

// Сбрасывает предпросмотр одежды
function resetClothes() {
    props.forEach(slot => player.clearProp(slot))
    mp.events.callRemote(Events.CS_RESET_CLOTHES)
}

// Предпросмотр вещи
function previewGood(item) {
    item = JSON.parse(item)
    if (item.drawable === -1 && item.isProp) return player.clearProp(item.slot)
    if (item.torso !== undefined) player.setComponentVariation(3, item.torso, 0, 0)
    if (item.undershirt !== undefined) player.setComponentVariation(8, item.undershirt, 0, 0)
    if (!item.isProp)
        player.setComponentVariation(item.slot, item.drawable, item.texture, 0)
    else
        player.setPropIndex(item.slot, item.drawable, item.texture, true)
}

// Перемещает камеру
function moveCamera(category) {
    let pos = cameraPositions.hatsPosition
    if (category === 0) pos = cameraPositions.hatsPosition
    else if (category === 11) pos = cameraPositions.topsPosition
    else if (category === 4) pos = cameraPositions.legsPosition
    else if (category === 6) pos = cameraPositions.feetsPosition
    else if (category === 1) pos = cameraPositions.hatsPosition
    else if (category === 2) pos = cameraPositions.hatsPosition
    else if (category === 60) pos = cameraPositions.topsPosition
    else if (category === 7) pos = cameraPositions.topsPosition
    dressingCamera.setCoord(pos.x, pos.y, pos.z)
}

// Обработчик поворота игрока
function headingChange(value) {
    let heading = defaultHeading - value
    player.setHeading(heading)
}