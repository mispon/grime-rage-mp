/*
** Вспомогательные штуки для разработки
*/
/*mp.events.add('render', () => {
    drawPosition()
})*/

// Отображает текущую позицию
function drawPosition() {
    let position = mp.players.local.position
    let x = position.x.toFixed(2)
    let y = position.y.toFixed(2)
    let z = position.z.toFixed(2)
    let h = mp.players.local.getHeading().toFixed(2)
    mp.game.graphics.drawText(
        `x: ${x}, y: ${y}, z: ${z}\r\nheading: ${h}`,
        [0.90, 0.90],
        {
            font: 7,
            color: [255, 255, 255, 225],
            scale: [0.4, 0.4],
            outline: true
        }
    )
}