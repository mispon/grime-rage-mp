const player = mp.players.local
const resolution = mp.game.graphics.getScreenActiveResolution(1, 1)
const widthHalf = resolution.x / 2
const heightHalf = resolution.x / 2

// Возвращает объект или сущность, на которую смотрит игрок
const getLookingAtEntity = () => {    
    let start = getStartPoint()
    let target = mp.game.graphics.screen2dToWorld3d([widthHalf, heightHalf, (2 | 4 | 8)])
    if (target == undefined) return null
    const result = mp.raycasting.testPointToPoint(start, target, player, (2 | 4 | 8 | 16))
    if (typeof result === 'undefined' || typeof result.entity.type === 'undefined') return null
    return isNear(result.entity) ? result.entity : null
}

// Возвращает начальную точку райкаста
function getStartPoint() {
    let result = player.getBoneCoords(12844, 0.5, 0, 0)
    result.z -= 0.3
    return result
}

// Находится ли игрок рядом с объектом пересечения
function isNear(entity) {
    let entPos = entity.position
    let lPos = player.position
    const distance = mp.game.gameplay.getDistanceBetweenCoords(entPos.x, entPos.y, entPos.z, lPos.x, lPos.y, lPos.z, true)
    return distance <= 4
}

export { getLookingAtEntity as lookingAt }