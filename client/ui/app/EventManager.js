var EventManager = {
    events: {},

    on(eventName, handler) {
        if (eventName in this.events)
            this.events[eventName].push(handler);        
        else 
            this.events[eventName] = [handler];
    }
}

function trigger(eventName, args) {
    var handlers = EventManager.events[eventName];
    try {
        var data = JSON.parse(args);
    } catch(e) {        
        mp.trigger('uiException', e.message);
    }
    handlers.forEach(handler => handler(data));
}