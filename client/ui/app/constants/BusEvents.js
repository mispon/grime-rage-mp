export const KEY_PRESSED = 'KeyPressed'
export const SHOW_RM_ITEM_VALUE = 'ShowRMItemValue'

// Компоненты
export const RESET = 'Component::Reset'
export const RANDOM = 'Component::Randomize'