import { KEY_PRESSED } from './constants/BusEvents'

import Vue from 'vue'
import App from './App.vue'

export const bus = new Vue()

new Vue({
    el: '#root',
    render: h => h(App)
})

// Обработчик клавиатуры для теста в браузере
document.onkeypress = function(e) {
    //bus.$emit(KEY_PRESSED, e)
}