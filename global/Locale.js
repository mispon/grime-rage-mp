const locales = {
    basic: {
        info: {ru: 'Информация', en: 'Info'},
        success: {ru: 'Успех', en: 'Success'},
        warning: {ru: 'Внимание', en: 'Warning'},
        error: {ru: 'Ошибка', en: 'Error'},
        fail: {ru: 'Неудача', en: 'Fail'},
        result: {ru: 'Результат', en: 'Result'},
        tip: {ru: 'Чаевые', en: 'Tip'},
        cost: {ru: 'Сумма', en: 'Cost'},
        color: {ru: 'Цвет', en: 'Color'},
    },
    button: {
        accept: {ru: 'Принять', en: 'Accept'},
        decline: {ru: 'Отклонить', en: 'Decline'},
        finish: {ru: 'Завершить', en: 'Finish'},
        create: {ru: 'Создать', en: 'Create'},
        random: {ru: 'Случайно', en: 'Random'},
        enter: {ru: 'Войти', en: 'Enter'},
        exit: {ru: 'Выйти', en: 'Exit'},
        register: {ru: 'Регистрация', en: 'Register'},
        next: {ru: 'Далее', en: 'Next'},
        dress: {ru: 'Одеть', en: 'Dress on'},
    },
    hint: {
        header: {
            ru: 'Подсказка',
            en: 'Hint'
        },
        npcTalk: {
            ru: 'Зажмите Е, чтобы поговорить',
            en: 'Hold E to talk'
        }
    },
    player: {
        noMoney: {
            ru: 'Недостаточно денег',
            en: 'Not enough money'
        },
        lowLevel: {
            ru: 'Минимальный необходимый уровень:',
            en: 'Minimum level required:'
        },
        levelUp: {
            ru: 'Вы достигли нового уровня!',
            en: 'You have reached a new level!'
        },
        noOwner: {
            ru: 'Вы не являетесь владельцем',
            en: 'You are not the owner'
        },
        hasLicense: {
            ru: 'У вас уже есть данная лицензия',
            en: 'You already have this license'
        },
        noLicense: {
            ru: 'У вас нет лицензии:',
            en: 'You do not have a license:'
        },
        newLicense: {
            ru: 'Вы получили новую лицензию',
            en: 'You have received a new license'
        },
        unavailableVehicle: {
            ru: 'Вы не можете управлять данным транспортом',
            en: 'You can not manage this transport'
        },
        startWorkAs: {
            ru: 'Вы начали работу:',
            en: 'You started work:',
        },
        alreadyWork: {
            ru: 'Вы уже работаете:',
            en: 'You are already working:'
        },
        finishWork: {
            ru: 'Вы завершили рабочую смену',
            en: 'You have completed a shift'
        },
        salary: {
            ru: 'Получена зарплата:',
            en: 'Salary received:',
        },
        penalty: {
            ru: 'Получен штраф:',
            en: 'Fine received:',
        },
        jobLevelUp: {
            ru: 'Ранг работы повышен до:',
            en: 'Work rank upgraded to:',
        },
        noContract: {
            ru: 'Вы не заключили контракт',
            en: 'You have not signed a contract'
        },
        lowJobRank: {
            ru: 'Недостаточный рабочий ранг',
            en: 'Insufficient working rank'
        },
        gift: {
            ru: 'Выдана награда за нахождение в игре',
            en: 'Awarded for being in the game'
        },
        gotPurchase: {
            ru: 'Вы приобрели',
            en: 'You purchased'
        }
    },
    vehicle: {
        noFuel: {
            ru: 'Бензобак пуст',
            en: 'The tank is empty'
        },
        noNearest: {
            ru: 'Поблизости нет транспорта',
            en: 'There is no transport nearby'
        },
        gotFuel: {
            ru: 'Транспорт успешно заправлен',
            en: 'Vehicle successfully filled'
        }
    },
    place: {
        drivingSchool: {ru: "Автошкола", en: "Driving school"},
        vehiclesRent: {ru: 'Аренда транспорта', en: 'Vehicle rental'},
        busDepot: {ru: 'Автобусный парк', en: 'Bus depot'},
        bistro: {ru: 'Закусочная', en: 'Bistro'},
        taxi: {ru: 'Таксопарк', en: 'Taxi station'},
        trucker: {ru: 'Грузоперевозка', en: 'Cargo transportation'},
        airport: {ru: 'Аэропорт', en: 'Airport'},
        filling: {ru: 'Заправка', en: 'Gas station'}
    },
    npc: {
        drivingSchool: {ru: "Инспектор Андреас", en: "Inspector Andreas"},
        vehiclesRent: {ru: 'Арендодатель', en: 'Landlord'},
        busDepot: {ru: 'Онэйл', en: 'Oneil'},
        bistro: {ru: 'Родригез', en: 'Rodriguez'},
        taxi: {ru: 'Луи', en: 'Lois'},
        sam: {ru: 'Сэм', en: 'Sam'},
        billy: {ru: 'Билли', en: 'Billy'},
        richard: {ru: 'Ричард', en: 'Richard'},
        cletus: {ru: 'Клетус', en: 'Cletus'},
        airWorker: {ru: 'Работник аэропорта', en: 'Airport worker'},
        airMech: {ru: 'Авиамеханик', en: 'Aircraft mechanic'},
        airFarmer: {ru: 'Фермер', en: 'Farmer'},
        airMilitary: {ru: 'Военный летчик', en: 'Military pilot'},
        shop: {ru: 'Продавец', en: 'Seller'}
    },
    jobs: {
        courier: {
            ru: 'Доставщик еды',
            en: 'Food courier'
        },
        busDriver: {
            ru: 'Водитель автобуса',
            en: 'Bus Driver'
        },
        taxi: {
            ru: 'Таксист',
            en: 'Taxi driver'
        },
        trucker: {
            ru: 'Дальнобойщик',
            en: 'Trucker'
        },
        pilot: {
            ru: 'Пилот',
            en: 'Pilot'
        },
        business: {
            ru: 'Бизнес',
            en: 'Business'
        }
    },
    busDriver: {
        toStart: {
            ru: 'Сядьте в автобус и двигайтесь по маршруту',
            en: 'Take the bus and follow the route'
        },
        ticketPay: {
            ru: 'Оплата билета:',
            en: 'Ticket payment:',
        },
        ticketBonus: {
            ru: 'Бонус от продажи билета',
            en: 'Bonus from ticket sales'
        },
        busStop: {
            ru: 'Остановка',
            en: 'Bus stop'
        }
    },
    courier: {
        target: {
            ru: 'Доставка',
            en: 'Delivery'
        },
        toStart: {
            ru: 'Сядьте на скутер, чтобы начать работу',
            en: 'Sit on a scooter to get started'
        },
        toComplete: {
            ru: 'Нажимте E, чтобы завершить доставку',
            en: 'Press E to complete delivery'
        }
    },
    taxiDriver: {
        toStart: {
            ru: 'Чтобы начать, выберите одну из машин такси',
            en: 'Select one of the taxi cars to get started'
        },
        order: {
            ru: 'Заказ такси',
            en: 'Taxi order'
        },
        waitOrder: {
            ru: 'Ожидание заказа...',
            en: 'Waiting for order ...'
        },
        hasOrder: {
            ru: 'Поступил заказ',
            en: 'Received order'
        },
        hurryingClient: {
            ru: 'Статус клиента: спешит',
            en: 'Client status: in a hurry'
        },
        hurryingClientChat: {
            ru: 'Клиент: Я опаздываю! Поспешите, пожалуйста!',
            en: 'Client: I am late! Hurry, please!'
        },
        calmClient: {
            ru: 'Статус клиента: не торопится',
            en: 'Client status: not in a hurry'
        },
        calmClientChat: {
            ru: 'Клиент: Надеюсь, вы аккуратный водитель',
            en: 'Client: I hope you are a neat driver'
        }
    },
    trucker: {
        toStart: {
            ru: 'Выберите контракт и грузовик на парковке',
            en: 'Choose a contract and a truck on the parking'
        },
        trailerControl: {
            ru: 'Зажмите H для управления прицепом',
            en: 'Clamp H to control the trailer'
        },
        needBeTrucker: {
            ru: 'Чтобы взять контракт, необходимо быть дальнобойщиком',
            en: 'To take the contract, you must be a truck driver'
        },
        alreadyHasContract: {
            ru: 'Вы уже взяли контракт на доставку груза',
            en: 'You have already taken delivery contract'
        },
        takedContract: {
            ru: 'Вы успешно взяли новый контракт',
            en: 'You have successfully taken a new contract'
        },
        toComplete: {
            ru: 'Зажмите H, чтобы завершить контракт',
            en: 'Clamp H to complete the contract'
        },
        target: {
            ru: 'Точка доставки',
            en: 'Delivery point'
        }
    },
    pilot: {
        toStart: {
            ru: 'Найдите самолёт на улице или в ангаре',
            en: 'Find a plane on the street or in the hangar'
        },
        needBePilot: {
            ru: 'Чтобы взять контракт, необходимо быть пилотом',
            en: 'To take the contract, you must be a pilot'
        },
        toComplete: {
            ru: 'Нажмите H, чтобы завершить контракт',
            en: 'Press H to complete the contract'
        },
    },
    contracts: {
        corn: {
            ru: 'Зерно',
            en: 'Corn'
        },
        crude: {
            ru: 'Сырье',
            en: 'Crude'
        },
        tools: {
            ru: 'Интрументы',
            en: 'Tools'
        },
        worksWood: {
            ru: 'Древесина (Мастерская)',
            en: 'Wood (Workshop)'
        },
        buildWood: {
            ru: 'Древесина (Стройка)',
            en: 'Wood (Building)'
        },
        dyes: {
            ru: 'Красители',
            en: 'Dyes'
        },
        fuel: {
            ru: 'Топливо',
            en: 'Fuel'
        },
        products: {
            ru: 'Продукты',
            en: 'Products'
        },
        scrapMetal: {
            ru: 'Металлолом',
            en: 'Scrap metal'
        },
        crop: {
            ru: 'Урожай',
            en: 'Crop'
        },
        mechDrawings: {
            ru: 'Чертежи механизмов',
            en: 'Mechanical Drawings'
        },
        fertilizer: {
            ru: 'Рассада и удобрения',
            en: 'Fertilizer'
        },
        militaryEquip: {
            ru: 'Военное снаряжение',
            en: 'Military Equipment'
        },
        prototypes: {
            ru: 'Собранные прототипы',
            en: 'Collected Prototypes'
        },
        militaryReports: {
            ru: 'Военные отчеты',
            en: 'Military Reports'
        }
    },
    radialMenu: {
        back: {ru: 'Назад', en: 'Back'},
        buy: {ru: 'Купить', en: 'Buy'},
        job: {ru: 'Работа', en: 'Job'},
        salary: {ru: 'Зарплата', en: 'Salary'},
        engine: {ru: 'Двигатель', en: 'Engine'},
        doors: {ru: 'Двери', en: 'Doors'},
        trunk: {ru: 'Багажник', en: 'Trunk'},
        bicycle: {ru: 'Велосипед', en: 'Bicycle'},
        scooter: {ru: 'Скутер', en: 'Scooter'},
        moto: {ru: 'Мотоцикл', en: 'Motorcycle'},
        car: {ru: 'Машина', en: 'Car'},
        theory: {ru: 'Теория', en: 'Theory'},
        category: {ru: 'Категория', en: 'Category'},
        shop: {ru: 'Магазин', en: 'Shop'},
        rent: {ru: 'Аренда', en: 'Rent'},
        contracts: {ru: 'Контракты', en: 'Contracts'},
        fuel: {ru: 'Бензин', en: 'Fuel'},
        clothes: {ru: 'Одежда', en: 'Clothes'}
    },
    filling: {
        noVehicle: {
            ru: 'Транспорт не найден',
            en: 'Vehicle not found'
        },
        noVehicleHint: {
            ru: 'Убедитесь, что поставили транспорт рядом с бензоколонкой',
            en: 'Be sure to put vehicle near the gas station'
        },
        fill: {ru: 'Заправить', en: 'Fill up'}
    },
    drivingSchool : {
        dutyDialogHeader: {
            ru: 'Вы согласны оплатить экзамен?',
            en: 'Do you agree to pay for the exam?'
        },
        dutyDialogDesc: {
            ru: 'Размер пошлины составляет:',
            en: 'The amount of duty is:'
        },
        bribeDialogHeader: {
            ru: 'Вы желаете приобрести лицензию?',
            en: 'Would you like to purchase a driver license??'
        },
        bribeDialogDesc: {
            ru: 'Стоимость моментальной покупки:',
            en: 'The cost of instant purchase:'
        },
        dutyPaid: {
            ru: 'Пошлина оплачена',
            en: 'Duty was paid'
        },
        theorySuccess: {
            ru: 'Вы успешно сдали теорию',
            en: 'You have successfully passed the theory'
        },
        fail: {
            ru: 'Вы не прошли экзамен. Сл. попытка:',
            en: 'You fail the exam. Next try:'
        },
        theoryAlreadyPassed: {
            ru: 'Вы уже сдали теорию',
            en: 'You have already passed the theory'
        },
        theoryNotPassed: {
            ru: 'Вы еще не сдали теорию',
            en: 'You have not yet passed the theory'
        },
        toStartPractice: {
            ru: 'Сядьте в подходящую машину, чтобы начать экзамен',
            en: 'Sit in the right machine to start the exam'
        },
        practiceWarn: {
            ru: 'Не врезайтесь и не превышайте скорость (60 км/ч)',
            en: 'Do not crash and do not exceed the speed (60 km/h)'
        },
        vehicleDamages: {
            ru: 'Количество повреждений:',
            en: 'Number of damage:'
        },
        overSpeed: {
            ru: 'Превышений скорости:',
            en: 'Number of speeding:'
        },
        timeNotYet: {
            ru: 'Дата следующей попытки:',
            en: 'Next attempt date:'
        },
        header: {
            ru: "Теоретический экзамен",
            en: "Theory exam"
        },        
        theoryQ1: {
            ru: "На какой цвет светофора разрешено движение?",
            en: "What color of traffic light is allowed to move?"
        },
        q1a1: {
            ru: "На зеленый",
            en: "On green"
        },
        q1a2: {
            ru: "На желтый",
            en: "On yellow"
        },
        q1a3: {
            ru: "На красный",
            en: "On red"
        },
        q1a4: {
            ru: "На любой",
            en: "On any"
        },    
        theoryQ2: {
            ru: "Разрешено ли садиться за руль в нетрезвом состоянии?",
            en: "Is it allowed to drive drunk?"
        },
        q2a1: {
            ru: "Да",
            en: "Yes"
        },
        q2a2: {
            ru: "Нет",
            en: "No"
        },
        q2a3: {
            ru: "Конечно же!",
            en: "Of course!"
        },
        q2a4: {
            ru: "Только если очень нужно",
            en: "Only if very necessary"
        },    
        theoryQ3: {
            ru: "За вами едет полицейская машина с включенными сиреной и мигалками. Ваши действия?",
            en: "For you goes a police car with the included siren and flashing lights. Your actions?"
        },
        q3a1: {
            ru: "Дам по газам!",
            en: "Increase speed!"
        },
        q3a2: {
            ru: "Продолжу ехать, как ехал",
            en: "I will continue to go as I was driving"
        },
        q3a3: {
            ru: "Остановлюсь у обочины",
            en: "Stop at the curb"
        },
        q3a4: {
            ru: "Заторможу на проезжей части",
            en: "I will stop on the carriageway"
        },    
        theoryQ4: {
            ru: "Разрешён ли обгон по встречной полосе?",
            en: "Is overtaking allowed in the oncoming lane?"
        },
        q4a1: {
            ru: "Нет, всегда запрещен",
            en: "No, always forbidden"
        },
        q4a2: {
            ru: "Да, только если полоса пуста и нет запрещающей разметки",
            en: "Yes, only if the bar is empty and there is no prohibition markup"
        },
        q4a3: {
            ru: "Да, даже если сзади меня едет автомобиль с мигалками",
            en: "Yes, even if a car flashes behind me"
        },
        q4a4: {
            ru: "Разрещен всегда",
            en: "Always allowed"
        },    
        theoryQ5: {
            ru: "Если на перекрёстке нет светофора и знака «Главная дорога», то кто должен ехать первым?",
            en: "If there is no traffic light and «Main road» sign at the intersection, who should go first?"
        },
        q5a1: {
            ru: "Тот, у кого нет помехи с правой стороны",
            en: "The one who has no interference on the right side"
        },
        q5a2: {
            ru: "Тот, у кого нет помехи с левой стороны",
            en: "The one who has no interference on the left side"
        },
        q5a3: {
            ru: "Любой желающий",
            en: "Anyone who wants"
        },
        q5a4: {
            ru: "Тот, кто приехал первый на перекрёсток",
            en: "The one who came first at the crossroads"
        },    
        theoryQ6: {
            ru: "Что означает мигание желтого сигнала светофора?",
            en: "Is overtaking allowed in the oncoming lane?"
        },
        q6a1: {
            ru: "Предупреждает о неисправности светофора",
            en: "Warns about a malfunction of the traffic light"
        },
        q6a2: {
            ru: "Запрещает дальнейшее движение",
            en: "Prohibits further movement"
        },
        q6a3: {
            ru: "Не знаю",
            en: "I do not know"
        },
        q6a4: {
            ru: "Разрешает движение и информирует о наличии нерегулируемого перекрестка",
            en: "Allows traffic and informs about the presence of an unregulated intersection"
        }
    },
    vehiclesRent: {
        rentHeader: {
            ru: 'Вы хотите арендовать выбранный транспорт?',
            en: 'Do you want to rent a selected vehicle?'
        },
        rentDesc: {
            ru: 'Стоимость аренды:',
            en: 'Rent price:'
        }
    },
    auth: {
        auth: {ru: 'Авторизация', en: 'Authorization'},
        register: {ru: 'Регистрация', en: 'Registration'},
        log: {ru: 'Логин', en: 'Login'},
        pass: {ru: 'Пароль', en: 'Password'},
        repass: {ru: 'Повтор пароля', en: 'Repeat password'},
        email: {ru: 'Почта', en: 'E-mail'},
        ref: {ru: 'Имя друга', en: 'Friend name'},
        opt: {ru: '(необяз.)', en: '(optional)'},
        logOrEmail: {ru: 'Логин или почта', en: 'Login or email'},
        wrongLogin: {ru: 'Неверный логин или пароль', en: 'Wrong login or password'},
        passNotMatch: {ru: 'Пароли не совпадают', en: 'Passwords do not match'},
        loginUsed: {ru: 'Логин уже используется', en: 'Login already in use'},
        emailUsed: {ru: 'Почта уже используется', en: 'Mail already in use'},
        emailInvalid: {ru: 'Введите корректную почту', en: 'Enter valid mail'},
        noEmpty: {ru: 'Заполните все поля', en: 'Fill in all the fields'},
        tooShortPass: {ru: 'В пароле должно быть 6 символов и более', en: 'The password must be 6 characters or more'}
    },
    character: {
        tooShort: {
            ru: 'Имя и фамилия должны быть длиннее двух символов',
            en: 'Name must be longer than two characters'
        },
        alreadyUse: {
            ru: 'Данное имя уже используется',
            en: 'This name is already in use'
        },
        none: {ru: 'Нет', en: 'None'},
        name: {ru: 'Имя', en: 'Name'},
        surname: {ru: 'Фамилия', en: 'Surname'},
        male: {ru: 'Мужчина', en: 'Male'},
        female: {ru: 'Женщина', en: 'Female'},
        genetic: {ru: 'Генетика', en: 'Genetic'},
        father: {ru: 'Отец', en: 'Father'},
        mother: {ru: 'Мать', en: 'Mother'},
        genes: {ru: 'Гены', en: 'Genes'},
        rotate: {ru: 'Поворот', en: 'Rotation'},
        skinColor: {ru: 'Цвет кожи', en: 'Skin color'},
        hair: {ru: 'Волосы', en: 'Hair'},
        face: {ru: 'Лицо', en: 'Face'},
        details: {ru: 'Детали', en: 'Details'},
        hairstyle: {ru: 'Прическа', en: 'Hairstyle'},
        hairColor: {ru: 'Цвет волос', en: 'Hair color'},
        highlightColor: {ru: 'Мелирование', en: 'Highlight color'},        
        beard: {ru: 'Борода', en: 'Beard'},
        beardColor: {ru: 'Цвет бороды', en: 'Beard color'},
        eyebrows: {ru: 'Брови', en: 'Eyebrows'},
        makeup: {ru: 'Косметика', en: 'Makeup'},
        blush: {ru: 'Румянец', en: 'Blush'},
        blushColor: {ru: 'Цвет румянца', en: 'Blush color'},
        lipstick: {ru: 'Помада', en: 'Lipstick'},
        lipstickColor: {ru: 'Цвет помады', en: 'Lipstick color'},
        freckles: {ru: 'Веснушки', en: 'Freckles'},
        tan: {ru: 'Загар', en: 'Tan'},
        complexion: {ru: 'Тип кожи', en: 'Complexion'},
        ageing: {ru: 'Старение', en: 'Ageing'},
        eyeColor: {ru: 'Цвет глаз', en: 'Eye Color'},
        blemishes: {ru: 'Дефекты', en: 'Blemishes'},
        chestHair: {ru: 'Волосы на груди', en: 'Сhest Hair'}
    },
    parents: {
        benjamin: {ru: 'Бэн', en: 'Benjamin'},
        daniel: {ru: 'Даниэль', en: 'Daniel'},
        joshua: {ru: 'Джошуа', en: 'Joshua'},
        noah: {ru: 'Ной', en: 'Noah'},
        andrew: {ru: 'Эндрю', en: 'Andrew'},
        juan: {ru: 'Джуан', en: 'Juan'},
        alex: {ru: 'Алекс', en: 'Alex'},
        isaac: {ru: 'Исаак', en: 'Isaac'},
        evan: {ru: 'Эван', en: 'Evan'},
        ethan: {ru: 'Итан', en: 'Ethan'},
        vincent: {ru: 'Винсент', en: 'Vincent'},
        angel: {ru: 'Энджел', en: 'Angel'},
        diego: {ru: 'Диего', en: 'Diego'},
        adrian: {ru: 'Ажриан', en: 'Adrian'},
        gabriel: {ru: 'Габриэль', en: 'Gabriel'},
        michael: {ru: 'Майкл', en: 'Michael'},
        santiago: {ru: 'Сантьяго', en: 'Santiago'},
        kevin: {ru: 'Кевин', en: 'Kevin'},
        louis: {ru: 'Луис', en: 'Louis'},
        samuel: {ru: 'Самуэль', en: 'Samuel'},
        anthony: {ru: 'Энтони', en: 'Anthony'},
        claude: {ru: 'Клод', en: 'Claude'},
        niko: {ru: 'Нико', en: 'Niko'},
        john: {ru: 'Джон', en: 'John'},
        hannah: {ru: 'Ханна', en: 'Hannah'},
        aubrey: {ru: 'Обри', en: 'Aubrey'},
        jasmine: {ru: 'Жасмин', en: 'Jasmine'},
        gisele: {ru: 'Джизель', en: 'Gisele'},
        amelia: {ru: 'Амелия', en: 'Amelia'},
        isabella: {ru: 'Изабелла', en: 'Isabella'},
        zoe: {ru: 'Зоя', en: 'Zoe'},
        ava: {ru: 'Ава', en: 'Ava'},
        camila: {ru: 'Камилла', en: 'Camila'},
        violet: {ru: 'Виолетта', en: 'Violet'},
        sophia: {ru: 'София', en: 'Sophia'},
        evelyn: {ru: 'Эвелин', en: 'Evelyn'},
        nicole: {ru: 'Николь', en: 'Nicole'},
        ashley: {ru: 'Эшли', en: 'Ashley'},
        gracie: {ru: 'Гарсия', en: 'Gracie'},
        brianna: {ru: 'Брианна', en: 'Brianna'},
        gatalie: {ru: 'Джетэль', en: 'Gatalie'},
        olivia: {ru: 'Оливия', en: 'Olivia'},
        elizabeth: {ru: 'Элизабет', en: 'Elizabeth'},
        charlotte: {ru: 'Шарлотта', en: 'Charlotte'},
        emma: {ru: 'Эмма', en: 'Emma'},
        misty: {ru: 'Мисти', en: 'Misty'}
    },
    hairs: {
        closeShave: {ru: 'Под ноль', en: 'Close Shave'},
        buzzcut: {ru: 'Ёжик', en: 'Buzzcut'},
        fauxHawk: {ru: 'Ястреб', en: 'Faux Hawk'},
        hipster: {ru: 'Хипстер', en: 'Hipster'},
        sideParting: {ru: 'Бритые бока', en: 'Side Parting'},
        shorterCut: {ru: 'Бритый', en: 'Shorter Cut'},
        biker: {ru: 'Байкер', en: 'Biker'},
        ponytail: {ru: 'Хвост', en: 'Ponytail'},
        cornrows: {ru: 'Дреды-косички', en: 'Cornrows'},
        slicked: {ru: 'Прилизанный', en: 'Slicked'},
        shortBrushed: {ru: 'Зачёс', en: 'Short Brushed'},
        spikey: {ru: 'Шипы', en: 'Spikey'},
        caesar: {ru: 'Цезарь', en: 'Caesar'},
        chopped: {ru: 'Локоны', en: 'Chopped'},
        dreads: {ru: 'Дреды', en: 'Dreads'},
        longHair: {ru: 'Длинные', en: 'Long Hair'},
        shaggyCurls: {ru: 'Лохматые кудри', en: 'Shaggy Curls'},
        surferDude: {ru: 'Сёрфер', en: 'Surfer Dude'},
        shortSide: {ru: 'Короткие бока', en: 'Short Side'},
        slickedSides: {ru: 'Футболист', en: 'High Slicked Sides'},
        longSlicked: {ru: 'Длинный зачёс', en: 'Long Slicked'},
        hipsterYouth: {ru: 'Молодой хипстер', en: 'Hipster Youth'},
        mullet: {ru: 'Маллет', en: 'Mullet'},
        classicCornrows: {ru: 'Косички (Классич.)', en: 'Classic Cornrows'},
        palmCornrows: {ru: 'Пальмовые дреды', en: 'Palm Cornrows'},
        lightCornrows: {ru: 'Лёгкие дреды', en: 'Lightning Cornrows'},
        whippedCornrows: {ru: 'Взбитые дреды', en: 'Whipped Cornrows'},
        zigZagCornrows: {ru: 'Зиг-заг дреды', en: 'Zig Zag Cornrows'},
        snailCornrows: {ru: 'Дреды-улитки', en: 'Snail Cornrows'},
        hightop: {ru: 'Высокий верх', en: 'Hightop'},
        looseSweptBack: {ru: 'Свободно назад', en: 'Loose Swept Back'},
        undercutSweptBack: {ru: 'Зачёс назад', en: 'Undercut Swept Back'},
        undercutSweptSide: {ru: 'Зачёс набок', en: 'Undercut Swept Side'},
        spikedMohawk: {ru: 'Шипастый эрокез', en: 'Spiked Mohawk'},
        mod: {ru: 'Модерн', en: 'Mod'},
        layeredMod: {ru: 'Модерн слоями', en: 'Layered Mod'},
        flattop: {ru: 'Площадка', en: 'Flattop'},
        militaryBuzzcut: {ru: 'Солдатский ёж', en: 'Military Buzzcut'},

        short: {ru: 'Коротко', en: 'Short'},
        layeredBob: {ru: 'Слои', en: 'Layered Bob'},
        pigtails: {ru: 'Косички', en: 'Pigtails'},
        braidedMohawk: {ru: 'Эрокез', en: 'Braided Mohawk'},
        braids: {ru: 'Косички', en: 'Braids'},
        bob: {ru: 'Боб', en: 'Bob'},
        frenchTwist: {ru: 'Ракушка', en: 'French Twist'},
        longBob: {ru: 'Лонг Боб', en: 'Long Bob'},
        looseTied: {ru: 'Свободно', en: 'Loose Tied'},
        pixie: {ru: 'Фея', en: 'Pixie'},
        shavedBangs: {ru: 'Подбритые виски', en: 'Shaved Bangs'},
        topKnot: {ru: 'Пучёк', en: 'Top Knot'},
        wavyBob: {ru: 'Волнистый Боб', en: 'Wavy Bob'},
        messyBun: {ru: 'Пучёк и локоны', en: 'Messy Bun'},
        pinUpGirl: {ru: 'Красотка', en: 'Pin Up Girl'},
        tightBun: {ru: 'Затянутый пучёк', en: 'Tight Bun'},
        twistedBob: {ru: 'Скрученный Боб', en: 'Twisted Bob'},
        flapperBob: {ru: 'Флэппер Боб', en: 'Flapper Bob'},
        bigBang: {ru: 'Взрыв', en: 'Big Bangs'},
        braidedTopKnot: {ru: 'Узел', en: 'Braided Top Knot'},
        pinchedCornrows: {ru: 'Пережатые дреды', en: 'Pinched Cornrows'},
        leafCornrows: {ru: 'Узор', en: 'Leaf Cornrows'},
        pigtailBangs: {ru: 'Косички с чёлкой', en: 'Pigtail Bangs'},
        waveBraids: {ru: 'Волнистые косы', en: 'Wave Braids'},
        coilBraids: {ru: 'Катушки', en: 'Coil Braids'},
        rolledQuiff: {ru: 'Чёлка-валик', en: 'Rolled Quiff'},
        bandana: {ru: 'Бандана', en: 'Bandana'},
        skinbyrd: {ru: 'Скинбёрд', en: 'Skinbyrd'},
        neatBun: {ru: 'Аккуратный пучёк', en: 'Neat Bun'},
        shortBob: {ru: 'Корокий Боб', en: 'Short Bob'}
    },
    beard: {
        lightStubble: {ru: 'Лёгкая щетина', en: 'Light Stubble'},
        balbo: {ru: 'Бальбо', en: 'Balbo'},
        circleBeard: {ru: 'Круглая борода', en: 'Circle Beard'},
        goatee: {ru: 'Эспаньолка', en: 'Goatee'},
        сhin: {ru: 'Козлиная', en: 'Chin'},
        сhinFuzz: {ru: 'Островок', en: 'Chin Fuzz'},
        pencilChinStrap: {ru: 'Тонкая', en: 'Pencil Chin Strap'},
        scruffy: {ru: 'Короткая', en: 'Scruffy'},
        musketeer: {ru: 'Мушкетер', en: 'Musketeer'},
        mustache: {ru: 'Усы', en: 'Mustache'},
        trimmedBeard: {ru: 'Подстриженная', en: 'Trimmed Beard'},
        stubble: {ru: 'Щетина', en: 'Stubble'},
        horseshoe: {ru: 'Подкова', en: 'Horseshoe'},
        pencilAndChops: {ru: 'Усы и баки', en: 'Pencil and Chops'},
        chinStrapBeard: {ru: 'Контурная', en: 'Chin Strap Beard'},
        balboAndSideburns: {ru: 'Бальбо и баки', en: 'Balbo and Sideburns'},
        muttonChops: {ru: 'Баки', en: 'Mutton Chops'},
        curly: {ru: 'Дали', en: 'Curly'},
        deepStranger: {ru: 'Дали и борода', en: 'Curly & Deep Stranger'},
        handlebar: {ru: 'Велосипедный руль', en: 'Handlebar'},
        faustic: {ru: 'Островок с усами', en: 'Faustic'},
        ottoPatch: {ru: 'Англ. усы с пеньком', en: 'Otto & Patch'},
        ottoFullStranger: {ru: 'Голливудская', en: 'Otto & Full Stranger'},
        lightFranz: {ru: 'Фу Манчу', en: 'Light Franz'},
        hampstead: {ru: 'Хэмпстед', en: 'The Hampstead'},
        ambrose: {ru: 'Широкие баки', en: 'The Ambrose'},
        lincolnCurtain: {ru: 'Линкольн', en: 'Lincoln Curtain'}
    },
    complexion: {
        rosyCheeks: {ru: 'Румянец', en: 'Rosy Cheeks'},
        stubbleRash: {ru: 'Раздражение от бритья', en: 'Stubble Rash'},
        hotFlush: {ru: 'Покраснение', en: 'Hot Flush'},
        sunburn: {ru: 'Солнечный ожог', en: 'Sunburn'},
        bruised: {ru: 'Синяки', en: 'Bruised'},
        alchoholic: {ru: 'Алкоголизм', en: 'Alchoholic'},
        patchy: {ru: 'Пятна', en: 'Patchy'},
        totem: {ru: 'Тотем', en: 'Totem'},
        bloodVessels: {ru: 'Кровеносные сосуды', en: 'Blood Vessels'},
        damaged: {ru: 'Повреждения', en: 'Damaged'},
        pale: {ru: 'Бледная', en: 'Pale'},
        ghostly: {ru: 'Призрачная', en: 'Ghostly'}
    },
    ageing: {
        сrowsFeet: {ru: 'Морщины у глаз', en: 'Crows Feet'},
        firstSigns: {ru: 'Признаки старения', en: 'First Signs'},
        middleAged: {ru: 'Средний возраст', en: 'Middle Aged'},
        worryLines: {ru: 'Морщины', en: 'Worry Lines'},
        depression: {ru: 'Депрессия', en: 'Depression'},
        distinguished: {ru: 'Преклонный возраст', en: 'Distinguished'},
        aged: {ru: 'Старость', en: 'Aged'},
        weathered: {ru: 'Обветренная кожа', en: 'Weathered'},
        wrinkled: {ru: 'Морщинистая кожа', en: 'Wrinkled'},
        sagging: {ru: 'Обвисшая кожа', en: 'Sagging'},
        toughLife: {ru: 'Тяжелая жизнь', en: 'Tough Life'},
        vintage: {ru: 'Винтаж', en: 'Vintage'},
        retired: {ru: 'Пенсионный возраст', en: 'Retired'},
        junkie: {ru: 'Наркомания', en: 'Junkie'},
        geriatric: {ru: 'Престарелость', en: 'Geriatric'}
    },
    eyeColor: {
        green: {ru: 'Зеленый', en: 'Green'},
        emerald: {ru: 'Изумрудный', en: 'Emerald'},
        lightBlue: {ru: 'Голубой', en: 'Light Blue'},
        oceanBlue: {ru: 'Синий океан', en: 'Ocean Blue'},
        lightBrown: {ru: 'Светло-коричневый', en: 'Light Brown'},
        darkBrown: {ru: 'Тёмно-коричневый', en: 'Dark Brown'},
        hazel: {ru: 'Карий', en: 'Hazel'},
        darkGray: {ru: 'Тёмно-серый', en: 'Dark Gray'},
        lightGray: {ru: 'Светло-серый', en: 'Light Gray'},
        pink: {ru: 'Розовый', en: 'Pink'},
        yellow: {ru: 'Желтый', en: 'Yellow'},
        purple: {ru: 'Фиолетовый', en: 'Purple'},
        blackout: {ru: 'Затемненный', en: 'Blackout'},
        shadesOfGray: {ru: 'Оттенок серого', en: 'Shades of Gray'},
        tequilaSunrise: {ru: 'Текила санрайз', en: 'Tequila Sunrise'},
        atomic: {ru: 'Атомный', en: 'Atomic'},
        warp: {ru: 'Деформи-рованный', en: 'Warp'},
        ecola: {ru: 'Е-Кола', en: 'ECola'},
        spaceRanger: {ru: 'Космический рейнджер', en: 'Space Ranger'},
        yingYang: {ru: 'Инь Янь', en: 'Ying Yang'},
        bullseye: {ru: 'Яблочко', en: 'Bullseye'},
        lizard: {ru: 'Ящерица', en: 'Lizard'},
        dragon: {ru: 'Дракон', en: 'Dragon'},
        extraTerrestrial: {ru: 'Внеземной', en: 'Extra Terrestrial'},
        goat: {ru: 'Козлиный', en: 'Goat'},
        smiley: {ru: 'Смайл', en: 'Smiley'},
        possessed: {ru: 'Одержимый', en: 'Possessed'},
        demon: {ru: 'Демон', en: 'Demon'},
        infected: {ru: 'Зараженный', en: 'Infected'},
        alien: {ru: 'Пришелец', en: 'Alien'},
        undead: {ru: 'Нежить', en: 'Undead'},
        zombie: {ru: 'Зомби', en: 'Zombie'}
    },
    eyebrows: {
        balanced: {ru: 'Аккуратные', en: 'Balanced'},
        fashion: {ru: 'Модные', en: 'Fashion'},
        cleopatra: {ru: 'Клеопатра', en: 'Cleopatra'},
        quizzical: {ru: 'Ироничные', en: 'Quizzical'},
        femme: {ru: 'Женственные', en: 'Femme'},
        seductive: {ru: 'Обольстительные', en: 'Seductive'},
        pinched: {ru: 'Нахмуренные', en: 'Pinched'},
        chola: {ru: 'Чикса', en: 'Chola'},
        triomphe: {ru: 'Торжествующие', en: 'Triomphe'},
        carefree: {ru: 'Беззаботные', en: 'Carefree'},
        curvaceous: {ru: 'Дугой', en: 'Curvaceous'},
        rodent: {ru: 'Мышка', en: 'Rodent'},
        doubleTram: {ru: 'Двойная высечка', en: 'Double Tram'},
        thin: {ru: 'Впалые', en: 'Thin'},
        penciled: {ru: 'Карандаш', en: 'Penciled'},
        motherPlucker: {ru: 'Выщипанные', en: 'Mother Plucker'},
        straightNarrow: {ru: 'Прямые тонкие', en: 'Straight and Narrow'},
        natural: {ru: 'Естественные', en: 'Natural'},
        fuzzy: {ru: 'Пышные', en: 'Fuzzy'},
        unkempt: {ru: 'Неопрятные', en: 'Unkempt'},
        caterpillar: {ru: 'Широкие', en: 'Caterpillar'},
        regular: {ru: 'Обычные', en: 'Regular'},
        mediterranean: {ru: 'Южноевропейские', en: 'Mediterranean'},
        groomed: {ru: 'Ухоженные', en: 'Groomed'},
        bushels: {ru: 'Кустистые', en: 'Bushels'},
        feathered: {ru: 'Перышки', en: 'Feathered'},
        prickly: {ru: 'Колючие', en: 'Prickly'},
        monobrow: {ru: 'Монобровь', en: 'Monobrow'},
        winged: {ru: 'Крылатые', en: 'Winged'},
        tripleTram: {ru: 'Тройная высечка', en: 'Triple Tram'},
        archedTram: {ru: 'Высечка дугой', en: 'Arched Tram'},
        cutouts: {ru: 'Подрезанные', en: 'Cutouts'},
        fadeAway: {ru: 'Сходящие', en: 'Fade Away'},
        soloTram: {ru: 'Высечка', en: 'Solo Tram'}
    },
    makeup: {
        smokyBlack: {ru: 'Дымчато-чёрный', en: 'Smoky Black'},
        bronze: {ru: 'Бронзовый', en: 'Bronze'},
        softGray: {ru: 'Мягкий серый', en: 'Soft Gray'},
        retroGlam: {ru: 'Ретро-гламур', en: 'Retro Glam'},
        naturalLook: {ru: 'Естественный', en: 'Natural Look'},
        catEyes: {ru: 'Кошачьи глаза', en: 'Cat Eyes'},
        chola: {ru: 'Чикса', en: 'Chola'},
        vamp: {ru: 'Вамп', en: 'Vamp'},
        vinewoodGlamour: {ru: 'Вайнвуд', en: 'Vinewood Glamour'},
        bubblegum: {ru: 'Баблгам', en: 'Bubblegum'},
        aquaDream: {ru: 'Морской', en: 'Aqua Dream'},
        pinUp: {ru: 'Пин-ап', en: 'Pin Up'},
        purplePassion: {ru: 'Лиловый', en: 'Purple Passion'},
        smokyCatEye: {ru: 'Дымчатый', en: 'Smoky Cat Eye'},
        smolderingRuby: {ru: 'Огненный', en: 'Smoldering Ruby'},
        popPrincess: {ru: 'Эстрадный', en: 'Pop Princess'}
    },
    freckles: {
        cherub: {ru: 'Ангелочек', en: 'Cherub'},
        allOver: {ru: 'Повсюду', en: 'All Over'},
        irregular: {ru: 'Местами', en: 'Irregular'},
        dotDash: {ru: 'Единичные', en: 'Dot Dash'},
        otb: {ru: 'На переносице', en: 'Over the Bridge'},
        babyDol: {ru: 'Куколка', en: 'Baby Doll'},
        pixie: {ru: 'Фея', en: 'Pixie'},
        sunKissed: {ru: 'Загорелая', en: 'Sun Kissed'},
        beautyMarks: {ru: 'Родинки', en: 'Beauty Marks'},
        lineUp: {ru: 'Ряд', en: 'Line Up'},
        modelesque: {ru: 'Как у модели', en: 'Modelesque'},
        occasional: {ru: 'Редкие', en: 'Occasional'},
        speckled: {ru: 'Веснушки', en: 'Speckled'},
        rainDrops: {ru: 'Капельки', en: 'Rain Drops'},
        doubleDip: {ru: 'Удвоенность', en: 'Double Dip'},
        oneSided: {ru: 'С одной стороны', en: 'One Sided'},
        pairs: {ru: 'Пары', en: 'Pairs'},
        growth: {ru: 'Бородавки', en: 'Growth'}
    },
    blush: {
        full: {ru: 'Полный', en: 'Full'},
        angled: {ru: 'Угловой', en: 'Angled'},
        round: {ru: 'Округлый', en: 'Round'},
        horiz: {ru: 'Горизонтально', en: 'Horizontal'},
        high: {ru: 'Высоко', en: 'High'},
        sweetheart: {ru: 'Влюблённый', en: 'Sweetheart'},
        eighties: {ru: 'Восьмидесятые', en: 'Eighties'}
    },
    lipstick: {
        cm: {ru: 'Цветная матовая', en: 'Color Matte'},
        cg: {ru: 'Цветная блестящая', en: 'Color Gloss'},
        lm: {ru: 'Контур, матовая', en: 'Lined Matte'},
        lg: {ru: 'Контур, блестящие', en: 'Lined Gloss'},
        hlm: {ru: 'Жирный контур, матовая', en: 'Heavy Lined Matte'},
        hlg: {ru: 'Жирный контур, блестящая', en: 'Heavy Lined Gloss'},
        lnm: {ru: 'Ненакрашенные матовые', en: 'Lined Nude Matte'},
        lng: {ru: 'Ненакрашенные блестящие', en: 'Liner Nude Gloss'},
        smudged: {ru: 'Размазанная', en: 'Smudged'},
        geisha: {ru: 'Гейша', en: 'Geisha'}
    },
    features: {
        noseWidth: {ru: 'Ширина носа', en: 'Nose Width'},
        noseBottomHeight: {ru: 'Высота носа', en: 'Nose Bottom Height'},
        noseTipLength: {ru: 'Длина кончика носа', en: 'Nose Tip Length'},
        noseBridgeDepth: {ru: 'Глубина переносицы', en: 'Nose Bridge Depth'},
        noseTipHeight: {ru: 'Высота кончика носа', en: 'Nose Tip Height'},
        noseBroken: {ru: 'Ломанность носа', en: 'Nose Broken'},
        browHeight: {ru: 'Высота бровей', en: 'Brow Height'},
        browDepth: {ru: 'Глубина бровей', en: 'Brow Depth'},
        cheekboneHeight: {ru: 'Высота скул', en: 'Cheekbone Height'},
        cheekboneWidth: {ru: 'Ширина скул', en: 'Cheekbone Width'},
        cheekDepth: {ru: 'Впалость щёк', en: 'Cheek Depth'},
        eyeSize: {ru: 'Размер глаз', en: 'Eye Size'},
        lipThickness: {ru: 'Толщина губ', en: 'Lip Thickness'},
        jawWidth: {ru: 'Ширина челюсти', en: 'Jaw Width'},
        jawShape: {ru: 'Форма челюсти', en: 'Jaw Shape'},
        chinHeight: {ru: 'Высота подбородка', en: 'Chin Height'},
        chinDepth: {ru: 'Глубина подбородка', en: 'Chin Depth'},
        chinWidth: {ru: 'Ширина подбородка', en: 'Chin Width'},
        chinIndent: {ru: 'Выраженность подбородка', en: 'Chin Indent'},
        neckWidth: {ru: 'Толщина шеи', en: 'Neck Width'}
    },
    dishes: {
        doughnut: {ru: 'Пончик', en: 'Doughnut'},
        cupcake: {ru: 'Кекс', en: 'Cupcake'},
        fries: {ru: 'Фри', en: 'Fries'},
        hotdog: {ru: 'Хот-Дог', en: 'Hot-Dog'},
        pizza: {ru: 'Пицца', en: 'Pizza'},
        chickleg: {ru: 'Куриная ножка', en: 'Chicken leg'},
        pancakes: {ru: 'Блинчики', en: 'Pancakes'},
        cake: {ru: 'Пирог', en: 'Cake'},
        kebab: {ru: 'Кебаб', en: 'Kebab'},
        burger: {ru: 'Бургер', en: 'Burger'},
        rice: {ru: 'Рис', en: 'Rice'},
        noodle: {ru: 'Лапша', en: 'Noodle'},
        water: {ru: 'Вода', en: 'Water'},
        cola: {ru: 'Кола', en: 'Cola'},
        soda: {ru: 'Содовая', en: 'Soda'},
        juice: {ru: 'Сок', en: 'Juice'},
        cocoa: {ru: 'Какао', en: 'Cocoa'},
        coffee: {ru: 'Кофе', en: 'Coffee'}
    },
    clothes: {
        dressingRoom: {ru: 'Примерочная', en: 'Dressing room'},
        category_0: {ru: 'Шапки', en: 'Hats'},
        category_11: {ru: 'Верхняя одежда', en: 'Tops'},
        category_4: {ru: 'Штаны', en: 'Legs'},
        category_6: {ru: 'Обувь', en: 'Feets'},
        category_1: {ru: 'Очки', en: 'Glasses'},
        category_2: {ru: 'Серьги', en: 'Ears'},
        category_60: {ru: 'Часы', en: 'Watches'},
        category_7: {ru: 'Браслеты', en: 'Bracelets'},
        cl_item_none: {ru: 'Отсутствует', en: 'None'},
        cl_item_01: {ru: 'Модная шапка', en: 'Stylish hat'},
        cl_item_02: {ru: 'Кепка LS', en: 'LS Cap'},
        cl_item_03: {ru: 'Шапка назад', en: 'Hat back'},
        cl_item_04: {ru: 'Рыбацкая кепка', en: 'Fishing cap'},
        cl_item_05: {ru: 'Козырек', en: 'Apron'},
        cl_item_06: {ru: 'Бандана', en: 'Bandana'},
        cl_item_07: {ru: 'Панама', en: 'Panama'},
        cl_item_08: {ru: 'Капитан Америка', en: 'Captain America'},
        cl_item_09: {ru: 'Новогодняя шапка', en: 'New Year hat'},
        cl_item_10: {ru: 'Кепка Naughty!', en: 'Naughty! cap'},
        cl_item_11: {ru: 'Кепка Naughty! назад', en: 'Naughty! cap back'},
        cl_item_12: {ru: 'Кепка King', en: 'King cap'},
        cl_item_13: {ru: 'Кепка', en: 'Cap'},
        cl_item_14: {ru: 'Кепка Масоны', en: 'Cap Masons'},
        cl_item_15: {ru: 'Низкая шапка', en: 'Low cap'},
        cl_item_16: {ru: 'Футболка', en: 'T-shirt'},
        cl_item_17: {ru: 'Майка', en: 'Mike'},
        cl_item_18: {ru: 'Кожанка', en: 'Leather jacket'},
        cl_item_19: {ru: 'Худи', en: 'Hoody'},
        cl_item_20: {ru: 'Рубашка с худи', en: 'Shirt with hoody'},
        cl_item_21: {ru: 'Куртка', en: 'Jacket'},
        cl_item_22: {ru: 'Поло', en: 'Polo'},
        cl_item_23: {ru: 'Рубашка', en: 'Shirt'},
        cl_item_24: {ru: 'Грязная футболка', en: 'Dirty t-short'},
        cl_item_25: {ru: 'Ветровка', en: 'Windbreaker'},
        cl_item_26: {ru: 'Короткая куртка', en: 'Short jacket'},
        cl_item_27: {ru: 'Балахон', en: 'Long hoody'},
        cl_item_28: {ru: 'Толстовка', en: 'Warm sweatshirt'},
        cl_item_29: {ru: 'Мастер Шэф', en: 'Master Chef'},
        cl_item_30: {ru: 'Большая футболка', en: 'Big t-shirt'},
        cl_item_31: {ru: 'Безрукавка', en: 'Sleeveless shirt'},
        cl_item_32: {ru: 'Джинсы', en: 'Jeans'},
        cl_item_33: {ru: 'Широкие джинсы', en: 'Wide jeans'},
        cl_item_34: {ru: 'Узкие джинсы', en: 'Skinny jeans'},
        cl_item_35: {ru: 'Брюки', en: 'Trousers'},
        cl_item_36: {ru: 'Штаны', en: 'Pants'},
        cl_item_37: {ru: 'Шорты', en: 'Shorts'},
        cl_item_38: {ru: 'Юбка', en: 'Skirt'},
        cl_item_39: {ru: 'Платье', en: 'Dress'},
        cl_item_40: {ru: 'Широкие штаны', en: 'Wide pants'},
        cl_item_41: {ru: 'Шорты с карманами', en: 'Pocket Shorts'},
        cl_item_42: {ru: 'Штаны с карманами', en: 'Pants with pockets'},
        cl_item_43: {ru: 'Спортивные штаны', en: 'Sweatpants'},
        cl_item_44: {ru: 'Бриджи', en: 'Breeches'},
        cl_item_45: {ru: 'Кеды', en: 'Gumshoes'},
        cl_item_46: {ru: 'Тапочки', en: 'Slippers'},
        cl_item_47: {ru: 'Кроссовки', en: 'Sneackers'},
        cl_item_48: {ru: 'Ботинки', en: 'Boots'},
        cl_item_49: {ru: 'Хайборды', en: 'Highbords'},
        cl_item_50: {ru: 'Простые очки', en: 'Plain glasses'},
        cl_item_51: {ru: 'Очки', en: 'Glasses'},
        cl_item_52: {ru: 'Большие очки', en: 'Big glasses'},
        cl_item_53: {ru: 'Узкие очки', en: 'Skinny glasses'},
        cl_item_54: {ru: 'Патриот 1', en: 'Patriot 1'},
        cl_item_55: {ru: 'Патриот 2', en: 'Patriot 2'},
        cl_item_56: {ru: 'Круглая серьга', en: 'Round earring'},
        cl_item_57: {ru: 'Серьга-кулон', en: 'Pendant Earring'},
        cl_item_58: {ru: 'Клипса', en: 'Clip'},
        cl_item_59: {ru: 'Череп', en: 'Skull'},
        cl_item_60: {ru: 'Шип', en: 'Spike'},
        cl_item_61: {ru: 'Электронные', en: 'Electronic'},
        cl_item_62: {ru: 'Дешевые', en: 'Poor'},
        cl_item_63: {ru: 'Механические', en: 'Mechanical'},
        cl_item_64: {ru: 'Простые', en: 'Common'},
        cl_item_65: {ru: 'Спортивные', en: 'Sports'},
        cl_item_66: {ru: 'Дедовские', en: 'Grandfathers'},
        cl_item_67: {ru: 'Цепь', en: 'Chain'},
        cl_item_68: {ru: 'Браслет', en: 'Bracelet'},
        cl_item_69: {ru: 'Черепа', en: 'Skulls'},
        cl_item_70: {ru: 'Шипастый', en: 'Spike'},
        cl_item_71: {ru: 'Кожаный', en: 'Leather'},
        cl_item_72: {ru: 'Канотье', en: 'Boater'},
        cl_item_73: {ru: 'Борсалино', en: 'Borsalino'},
        cl_item_74: {ru: 'Котелок', en: 'Bowler'},
        cl_item_75: {ru: 'Высокий котелок', en: 'High bowler'},
        cl_item_76: {ru: 'Твидовая шляпа', en: 'Tweed hat'},
        cl_item_77: {ru: 'Трилби', en: 'Trilby'},
        cl_item_78: {ru: 'Федора', en: 'Fedora'},
        cl_item_79: {ru: 'Распахнутый пиджак', en: 'Open jacket'},
        cl_item_80: {ru: 'Застегнутый пиджак', en: 'Buttoned jacket'},
        cl_item_81: {ru: 'Жилет', en: 'Vest'},
        cl_item_82: {ru: 'Заправленная рубашка', en: 'Tucked shirt'},
        cl_item_83: {ru: 'Фрак', en: 'Tail coat'},
        cl_item_84: {ru: 'Пиджак и жилет', en: 'Jacket and vest'},
        cl_item_85: {ru: 'Рубашка с подтяжками', en: 'Shirt with suspenders'},
        cl_item_86: {ru: 'Пуховик', en: 'Down jacket'},
        cl_item_87: {ru: 'Большое пальто', en: 'Big coat'},
        cl_item_88: {ru: 'Плащ', en: 'Cloak'},
        cl_item_89: {ru: 'Свитер', en: 'Sweater'},
        cl_item_90: {ru: 'Изысканное пальто', en: 'Exquisite coat'},
        cl_item_91: {ru: 'Пальто и жилет', en: 'Coat and Vest'},
        cl_item_92: {ru: 'Полосатое пальто', en: 'Striped coat'},
        cl_item_93: {ru: 'Узкие брюки', en: 'Tight pants'},
        cl_item_94: {ru: 'Мокасины', en: 'Moccasins'},
        cl_item_95: {ru: 'Туфли', en: 'Shoes'},
        cl_item_96: {ru: 'Бриллиантовая клипса', en: 'Diamond clip'},
        cl_item_97: {ru: 'Золотые символы', en: 'Golden symbols'},
        cl_item_98: {ru: 'Черный алмаз', en: 'Black Diamond'},
        cl_item_99: {ru: 'Платиновая клипса', en: 'Platinum clip'},
        cl_item_100: {ru: 'Золотая клипса', en: 'Golden clip'},
        cl_item_101: {ru: 'Золотые часы', en: 'Gold watch'},
        cl_item_102: {ru: 'Платиновые часы', en: 'Platinum watch'},
        cl_item_103: {ru: 'Из красного дерева', en: 'Mahogany watch'},
        cl_item_104: {ru: 'Бриллиантовые часы', en: 'Diamond watch'},
        cl_item_105: {ru: 'Матовые часы', en: 'Matte watch'},
        cl_item_106: {ru: 'Амулеты', en: 'Amulets'},
        cl_item_107: {ru: 'Кепи', en: 'Caps'},
        cl_item_108: {ru: 'Берет', en: 'Beret'},
        cl_item_109: {ru: 'Пляжная шляпка', en: 'Beach hat'},
        cl_item_110: {ru: 'Шляпа', en: 'Hat'},
        cl_item_111: {ru: 'Летнее платье', en: 'Summer dress'},
        cl_item_112: {ru: 'Блузка', en: 'Blouse'},
        cl_item_113: {ru: 'Короткий жакет', en: 'Short jacket'},
        cl_item_114: {ru: 'Балетки', en: 'Ballet shoes'},
        cl_item_115: {ru: 'Пальто', en: 'Coat'},
        cl_item_116: {ru: 'Закрытая блузка', en: 'Closed blouse'},
        cl_item_117: {ru: 'Сапоги на каблуке', en: 'Boots with heels'},
        cl_item_118: {ru: 'Туфли на шпильке', en: 'Stilettos'},
        cl_item_119: {ru: 'Туфли на платформе', en: 'Platform shoes'},
        cl_item_120: {ru: 'Модные очки', en: 'Fashionable glasses'},
        cl_item_121: {ru: 'Платиновые серьги', en: 'Platinum earrings'},
        cl_item_122: {ru: 'Золотые серьги', en: 'Gold earrings'},
        cl_item_123: {ru: 'Золотые кольца', en: 'Golden rings'},
        cl_item_124: {ru: 'Золотой браслет', en: 'Gold bracelet'},
        cl_item_125: {ru: 'Бриллиантовый браслет', en: 'Diamond bracelet'},
        cl_item_126: {ru: 'Кепка назад', en: 'Cap back'},
        cl_item_127: {ru: 'Висячие серьги', en: 'Dangling earrings'},
        cl_item_128: {ru: 'АК 47', en: 'AK 47'},
        cl_item_129: {ru: 'Круглые с надписью', en: 'Round with inscription'}
    }
}

// Возвращает значение в нужной локализации
const get = (type, value, lang) => {
    if (!lang) lang = tryGetLang()
    const result = locales[type][value]
    if (!result[lang]) return result.en
    return result[lang]
}

// Безопасное получение настройки языка
// NOTE: mp.config.lang - работает только на сервере!
const tryGetLang = () => {
    try {
        return mp.config.lang
    } catch(e) {
        return 'en'
    }
}

export { get as locale }