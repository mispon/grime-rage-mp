/*
** Константы всех эвентов сервера
*/

// Общие
export const GUI_READY = 'GuiReady'
export const SET_LANG = 'SetLang'
export const CREATE_NPCS = 'CreateNpcs'
export const SHOW_DELIVERY_CONTRACTS = 'ShowDeliveryContracts'
export const SET_GIFT_TIMER = 'SetGiftTimer'
export const GIVE_GIFT = 'GiveGift'

// Игрок и его hud
export const SET_TIMER = 'SetTimer'
export const RESET_TIMER = 'ResetTimer'
export const SHOW_HUD = 'HUD::Show'
export const UPDATE_HUD = 'HUD::Update'
export const HUD_SET_MICRO = 'HUD::SetMicro'
export const HUD_TOGGLE_VEHICLE = 'HUD::ToggleVehicle'
export const HUD_UPDATE_VEHICLE = 'HUD::UpdateVehicle'
export const HUD_SET_STREET = 'HUD::SetStreet'
export const HUD_TOGGLE = 'HUD::Toggle'

// Транспорт
export const TOGGLE_ENGINE = 'ToggleEngine'
export const TOGGLE_LOCK = 'ToggleLock'
export const SET_FUEL = 'SetFuel'

// Проксирующие эвенты
export const CALL_REMOTE = 'CallRemote'
export const CALL_UI = 'CallUI'

// Радиальное меню
export const SETUP_MENU = 'RM::SetupMenu'
export const TOGGLE_MENU = 'RM::ToggleMenu'

// Боковая панель
export const SHOW_SIDEBAR = 'SB::Show'
export const HIDE_SIDEBAR = 'SB::Hide'

// Уведомления
export const SHOW_ALERT = 'ShowAlert'
export const SHOW_HINT = 'ShowHint'
export const HIDE_HINT = 'HideHint'
export const SHOW_DIALOG = 'ShowDialog'
export const DIALOG_SELECT = 'DialogSelect'

// Голосовой чат
export const ADD_LISTENER = 'AddListener'
export const REMOVE_LISTENER = 'RemoveListener'

// Закусочная
export const BISTRO_SHOW = 'Bistro::Show'
export const BISTRO_HIDE = 'Bistro::Hide'
export const BISTRO_ORDER = 'Bistro::Order'

// Курьер
export const COURIER_WORK = 'Courier::Work'
export const COURIER_SALARY = 'Courier::Salary'
export const COURIER_NEXT = 'Courier::NextTarget'
export const COURIER_HIDE = 'Courier::HideTarget'
export const DELIVERY_TIME_OUT = 'Courier::TimeIsOut'
export const COMPLETE_DELIVERY = 'Courier::Complete'

// Водитель автобуса
export const BD_WORK = 'BD::Work'
export const BD_SALARY = 'BD::Salary'
export const BD_NEXT_POINT = 'BD::NextPoint'
export const BD_HIDE_ROUTE = 'BD::HideRoute'

// Таксист
export const TAXI_WORK = 'Taxi::Work'
export const TAXI_SALARY = 'Taxi::Salary'
export const TAXI_WAIT_ORDER = 'Taxi::WaitOrder'
export const TAXI_HAS_ORDER = 'Taxi::HasOrder'
export const TAXI_START_ORDER = 'Taxi::StartOrder'
export const TAXI_COMPLETE_ORDER = 'Taxi::CompleteOrder'
export const TAXI_REMOVE_ORDER = 'Taxi::RemoveOrder'
export const TAXI_TIME_IS_OUT = 'Taxi::TimeOut'
export const TAXI_OVER_SPEED = 'Taxi::OverSpeed'

// Дальнобойщик
export const TRUCKER_WORK = 'Trucker::Work'
export const TRUCKER_SALARY = 'Trucker::Salary'
export const TRUCKER_SELECT_CONTRACT = 'Trucker::SelectContract'
export const TRUCKER_SHOW_TARGET = 'Trucker::ShowTarget'
export const TRUCKER_HIDE_TARGET = 'Trucker::HideTarget'
export const TRUCKER_ON_TARGET = 'Trucker::TargetPoint'

// Пилот
export const PILOT_WORK = 'Pilot::Work'
export const PILOT_SALARY = 'Pilot::Salary'
export const PILOT_SELECT_CONTRACT = 'Pilot::SelectContract'
export const PILOT_SHOW_TARGET = 'Pilot::ShowTarget'
export const PILOT_HIDE_TARGET = 'Pilot::HideTarget'
export const PILOT_COMPLETE_ORDER = 'Pilot::Complete'

// Аренда транспорта
export const RENT_VEHICLE = 'RentVehicle'

// Автошкола
export const BUY_LICENSE = 'DS::BuyLicense'
export const START_THEORY_EXAM = 'DS::StartTheoryExam'
export const THEORY_EXAM_FINISH = 'DS::TheoryExamFinish'
export const START_PRACTICE_EXAM = 'DS::StartPracticeExam'
export const DS_OVER_SPEED = 'DS::OverSpeed'
export const NEXT_EXAM_POINT = 'DS::NextPoint'
export const HIDE_EXAM_POINT = 'DS::HidePoint'

// Заправка
export const FILL_VEHICLE = 'FillVehicle'
export const FILL_CANISTER = 'FillCanister'
export const FS_SHOP_BUY = 'FS::ShopBuy'
export const FS_SHOW = 'FS::Show'
export const FS_SHOW_SHOP = 'FS::ShowShop'

// Магазин одежды
export const CS_ENTER_DR = 'CS::EnterDressingRoom'
export const CS_LEAVE_DR = 'CS::LeaveDressingRoom'
export const CS_PREVIEW_GOOD = 'CS::PreviewGood'
export const CS_RESET_CLOTHES = 'CS::ResetClothes'
export const CS_CONFIRM_PURCHASE = 'CS::ConfirmPurchase'
export const CS_NO_MONEY = 'CS::NoMoney'
export const CS_BUY = 'CS::Buy'
export const CS_MOVE_CAMERA = 'CS::MoveCamera'
export const CS_ROTATE = 'CS::Rotate'

// Авторизация
export const AUTH_SHOW = 'Auth::Show'
export const AUTH_HIDE = 'Auth::Hide'
export const AUTH_LOGIN = 'Auth::Login'
export const AUTH_REGISTER = 'Auth::Register'
export const AUTH_LOGIN_FAIL = 'Auth::LoginFail'
export const AUTH_REGISTER_FAIL = 'Auth::RegisterFail'

// Создание персонажа
export const CHAR_SHOW_CREATOR = "CharCr::Show"
export const CHAR_HIDE_CREATOR = "CharCr::Hide"
export const CHAR_CREATE = "CharCr::Create"
export const CHAR_NAME_USE = "CharCr::NameAlreadyUse"
export const CHAR_PREVIEW = "CharCr::Preview"
export const CHAR_GENDER_CHANGE = "CharCr::GenderChange"
export const CHAR_ROTATE = "CharCr::Rotate"

// Окно админа
export const TOGGLE_ADMIN_WINDOW = 'Admin::Toggle'
export const CREATE_VEHICLE = 'Admin::CreateVehicle'
export const GIVE_WEAPON = 'Admin::GiveWeapon'
export const LOG_POSITION = 'Admin::LogPosition'
export const TELEPORT = 'Admin::Teleport'
export const TEST = 'Admin::Test'