/*
** Константы транспорта
*/

// Константы общего транспорта
export const DRIVER_SEAT = -1
export const ONE_KILOMETER = 500.0
export const GENERAL_VEHICLE_KEY = 'GeneralVehicle'
export const DRIVER_KEY = 'VehicleDriver'