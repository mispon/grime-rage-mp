/*
** Виды различных лицензий
*/
const LicenseType = Object.freeze({
    "lightVehicle": 1,
    "heavyVehicle": 2,
    "waterVehicle": 3,
    "airVehicle": 4,
    "weapon": 5,
})

export default LicenseType