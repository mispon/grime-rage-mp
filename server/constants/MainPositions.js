/*
** Основные позиции сервера
*/
export const DrivingSchool = new mp.Vector3(68.83, -1569.63, 29.60)
export const BusDepot = new mp.Vector3(45.55, -843.46, 30.87)
export const Bistro = new mp.Vector3(154.91, -1431.74, 29.25)
export const TaxiStation = new mp.Vector3(212.36, -808.72, 30.81)
export const Port = new mp.Vector3(1085.38, -3193.89, 5.90)
export const AirPort = new mp.Vector3(-1042.98, -2746.27, 21.36)