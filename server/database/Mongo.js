const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')

mongoose.connect(`mongodb://${mp.config.db}:27017/Grime`, {useNewUrlParser: true})

mongoose.connection.on('connected', () => {
    console.log('[MONGOOSE]: Successfuly connected')
})

mongoose.connection.on('error', error => {
    console.log(`[MONGOOSE]: Connection failed: ${error}`)
})

module.exports = mongoose