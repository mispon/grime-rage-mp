import PlayerModel from '../schemas/PlayerSchema'
import Misc from '../../utils/Misc'
import crypto from 'crypto'

/*
** Провайдер к данным игрока
*/
class PlayerProvider {
    constructor() {
        if (!PlayerProvider.instance) {
            PlayerProvider.instance = this
        }
        return PlayerProvider.instance
    }

    // Авторизирует игрока
    async login(player, login, password) {
        const filter = {$or: [{'account.login': login}, {'account.email': login}]}
        let data = await PlayerModel.findOne(filter)
        if (!(data && isPassEquals(data.account, password))) return false
        player.props = data
        return true
    }

    // Создает новый аккаунт
    async register(player, login, password, email, referal) {
        const checkResult = await this.checkOnExist(login, email)
        if (checkResult) return checkResult
        let emptyModel = await this.createNew(login, password, email, referal)
        emptyModel.account.created = new Date()
        player.props = emptyModel        
        await emptyModel.save()
        return ''
    }

    // Создает новый аккаунт
    async createNew(login, password, email, referal) {
        const salt = Misc.randomString(30)
        password = encryptPassword(password, salt)
        return new PlayerModel({
            id: await getMaxId(),
            account: {login, password, salt, email, referal},
            driver: {isDutyPaid: false, isTheoryPassed: false},
            money: 150, level: 1, exp: 0, satiety: 100, thirst: 100
        })
    }

    // Проверяет, существует ли уже аккаунт с введенными данными
    async checkOnExist(login, email) {
        let account = await PlayerModel.findOne({'account.login': login})
        if (account) return 'loginUsed'
        if (email) {
            account = await PlayerModel.findOne({'account.email': email})
            if (account) return 'emailUsed'
        }
        return null
    }

    // Сохраняет данные игрока
    async save(player) {
        player.props.health = player.health
        player.props.position = player.position
        player.props.dimension = player.dimension
        await player.props.save()       
    }

    // Проверяет, используется ли данное имя
    async isNameUsed(name) {
        let user = await PlayerModel.findOne({name})
        return user !== null
    }
}

// Возвращает максимальный идентификатор
async function getMaxId() {
    const result = await PlayerModel.find({}, ['id'], {skip: 0, limit: 1, sort: {id: -1}})
    return result.length > 0 ? result[0].id + 1 : 1
}

// Возвращает зашифрованный пароль
const encryptPassword = (password, salt) => {
    return crypto.createHmac('sha256', salt).update(password).digest('hex')
}

// Проверяет, совпадают ли пароли пароля
const isPassEquals = (account, password) => {
    return account.password === encryptPassword(password, account.salt)
}

const instance = new PlayerProvider()

export default instance