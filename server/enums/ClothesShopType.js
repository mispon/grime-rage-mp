/*
** Типы магазинов одежды
*/
const ClothesShopType = Object.freeze({
    SubUrban: 0,
    Ponsonbys: 1
})

export default ClothesShopType