/*
** Типы работ
*/
const JobType = Object.freeze({
    business: 0,
    courier: 1,
    busDriver: 2,
    taxi: 3,
    trucker: 4,
    pilot: 5
})

export default JobType