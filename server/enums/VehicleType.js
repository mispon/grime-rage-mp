const VehicleType = Object.freeze({
    drivingSchool: 'DrivingSchool',
    scooter: 'Scooter',
    bus: 'Bus',
    taxi: 'Taxi',
    truck: 'Truck',
    plane: 'Plane'
})

export default VehicleType