import { GUI_READY, CALL_UI, SET_LANG, AUTH_SHOW } from '../../global/constants/ServerEvents'
import { fastLogin } from '../managers/auth/AuthManager'
import PointCreator from '../utils/PointCreator'

const devs = ['Mispon']

mp.events.add({
    [GUI_READY]: onGuiReady,
    ['playerJoin']: onPlayerJoin
})

// Обработчик готовности интерфейса
function onGuiReady(player) {
    const lang = mp.config.lang
    player.call(CALL_UI, [SET_LANG, lang])
    if (!player.logged) player.call(AUTH_SHOW)
}

// Обработчик подключения на сервер
function onPlayerJoin(player) {
    PointCreator.sendNpcsData(player)    
    if (devs.includes(player.name)) {
        const acc = getAuthData(player.name)
        fastLogin(player, acc.login, acc.password)
    }
}

// Возвращает логин и пароль для быстрой авторизации
function getAuthData(name) {
    switch(name) {
        case 'Mispon': return {login: 'mispon', password: 'devdev'}
    }
}
