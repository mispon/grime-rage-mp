/*
** Обработчик выхода с сервера
*/
mp.events.add('playerQuit', player => {
    console.log(`${player.name} вышел с сервера`)
})