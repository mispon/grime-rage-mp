import { HUD_TOGGLE_VEHICLE } from '../../global/constants/ServerEvents'
import { DRIVER_SEAT } from '../../global/constants/VehicleVariables'

/*
** Эвенты транспорта
*/
mp.events.add({
    'playerEnterVehicle': onEnter,
    'playerStartExitVehicle': onStartExit,
    'playerExitVehicle': onExit
})

// Общий обработчик входа в транспорт
function onEnter(player, vehicle, seat) {
    if (seat !== DRIVER_SEAT) return
    vehicle.setDriver(player)
    const data = [true, vehicle.props.fuel, vehicle.props.fuelTank, vehicle.props.fuelRate]
    player.call(HUD_TOGGLE_VEHICLE, data)
    vehicle.props.afk = null
}

// Общий обработчик начала выхода из транспорта
function onStartExit(player) {
    if (player.seat !== DRIVER_SEAT) return
    player.vehicle.setDriver(null)
    player.vehicle.props.afk = Date.now()
}

// Общий обработчик выхода из транспорта
function onExit(player) {
    player.call(HUD_TOGGLE_VEHICLE, [false])
}