import * as Events from '../../../global/constants/ServerEvents'
import { LANG } from '../../../global/constants/PlayerVariables'
import { applyAppearance, startCreation } from './CharacterCreator'
import PlayerBehavior from '../player/behaviors/PlayerBehavior'
import PlayerProvider from '../../database/providers/PlayerProvider'

/*
** Серверная логика регистрации и авторизации
*/

mp.events.add({
    [Events.AUTH_LOGIN]: login,
    [Events.AUTH_REGISTER]: register
})

// Обработчик логина
function login(player, login, password) {
    PlayerProvider.login(player, login, password)
        .then(result => {
            if (result) 
                onAuth(player)
            else
                player.call(Events.CALL_UI, [Events.AUTH_LOGIN_FAIL])
        })
        .catch(reason => console.log(`Login error: ${reason}`))
}

// Обработчик регистрации
function register(player, data) {
    data = JSON.parse(data)
    PlayerProvider.register(player, data.login, data.password, data.email, data.referal)
        .then(result => {
            if (result !== '') {
                return player.call(Events.CALL_UI, [Events.AUTH_REGISTER_FAIL, result])
            }
            giveBehavior(player)
            startCreation(player)
        })
        .catch(reason => console.log(`Register error: ${reason}`))
}

// Обработчик успешной авторизации
function onAuth(player) {   
    giveBehavior(player)
    if (player.props.name) {
        player.logged = true
        applyAppearance(player)
        player.applyCurrentClothes()
        player.call(Events.CALL_UI, [Events.SHOW_HUD])
    }
    else
        startCreation(player)
}

// Добавляет дополнительное поведение игроку
function giveBehavior(player) {
    new PlayerBehavior(player)
    player.set(LANG, mp.config.lang)
    player.call(Events.AUTH_HIDE)
}

// Быстрая авторизация для разработки
export const fastLogin = (player, log, pass) => login(player, log, pass)