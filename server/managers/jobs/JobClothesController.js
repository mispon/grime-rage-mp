import JobType from '../../enums/JobType'

/*
** Логика рабочей одежды
*/

// Одеть игрока в рабочую одежду
export const applyWorkClothes = player => {
    if (!player.props.job) return
    switch(player.props.job.type) {
        case JobType.courier:
            setCourierClothes(player)
            break
        case JobType.busDriver:
            setBusDriverClothes(player)
            break
        case JobType.taxi:
            setTaxiClothes(player)
            break
        case JobType.trucker:
            setTruckerClothes(player)
            break
        case JobType.pilot:
            setPilotClothes(player)
            break
    }
}

// Одеть одежду курьера закусочной
function setCourierClothes(player) {
    const isMale = player.isMale()
    const clothes = [
        {slot: 0, drawable: isMale ? 45 : 44, texture: 0, isProp: true},
        {slot: 11, drawable: isMale ? 1 : 14, torso: isMale ? 0 : 14, undershirt: isMale ? 57 : 3, texture: 4},
        {slot: 4, drawable: isMale ? 12 : 25, texture: 0},
        {slot: 6, drawable: isMale ? 1 : 3, texture: 0},
        {slot: 5, drawable: 45, texture: 0}
    ]
    player.applyTempClothes(clothes)
}

// Одеть одежду водителя автобуса
function setBusDriverClothes(player) {
    const isMale = player.isMale()
    const clothes = [
        {slot: 0, drawable: isMale ? 7 : 5, texture: 0, isProp: true},
        {slot: 11, drawable: isMale ? 6 : 69, torso: isMale ? 14 : 0, undershirt: isMale ? 24 : 58, texture: 0},
        {slot: 4, drawable: isMale ? 5 : 1, texture: 0},
        {slot: 6, drawable: isMale ? 6 : 4, texture: 0}
    ]
    player.applyTempClothes(clothes)
}

// Одеть одежду таксиста
function setTaxiClothes(player) {
    const isMale = player.isMale()
    const clothes = [
        {slot: 0, drawable: isMale ? 7 : 5, texture: 0, isProp: true},
        {slot: 11, drawable: isMale ? 6 : 69, torso: isMale ? 14 : 0, undershirt: isMale ? 24 : 58, texture: 0},
        {slot: 4, drawable: isMale ? 5 : 1, texture: 0},
        {slot: 6, drawable: isMale ? 6 : 4, texture: 0}
    ]
    player.applyTempClothes(clothes)
}

// Одеть одежду дальнобойщика
function setTruckerClothes(player) {
    const isMale = player.isMale()
    const clothes = [
        {slot: 0, drawable: isMale ? 7 : 5, texture: 0, isProp: true},
        {slot: 11, drawable: isMale ? 6 : 69, torso: isMale ? 14 : 0, undershirt: isMale ? 24 : 58, texture: 0},
        {slot: 4, drawable: isMale ? 5 : 1, texture: 0},
        {slot: 6, drawable: isMale ? 6 : 4, texture: 0}
    ]
    player.applyTempClothes(clothes)
}

// Одеть одежду лётчика
function setPilotClothes(player) {
    const isMale = player.isMale()
    const clothes = [
        {slot: 0, drawable: isMale ? 47 : 37, texture: 0, isProp: true},
        {slot: 11, drawable: isMale ? 54 : 47, torso: isMale ? 1 : 3, undershirt: isMale ? 57 : 3, texture: 0},
        {slot: 4, drawable: isMale ? 41 : 42, texture: 0},
        {slot: 6, drawable: 24, texture: 0}
    ]
    player.applyTempClothes(clothes)
}