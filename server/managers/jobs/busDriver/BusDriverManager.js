import data from './bus-depot-data.json'
import { BD_NEXT_POINT, BD_HIDE_ROUTE } from '../../../../global/constants/ServerEvents'
import { DRIVER_SEAT } from '../../../../global/constants/VehicleVariables'
import { IS_BUS_DRIVER } from '../../../../global/constants/PlayerVariables'
import VehicleTypes from '../../../enums/VehicleType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import { locale } from '../../../../global/Locale'

/*
** Логика работы водителем автобуса
*/

export const POINT_TYPE = 'RoutePointType'
export const POINT_INDEX = 'RoutePointIndex'
const TICKET_COST = 10
const POINT_FLAG = 'PlayerOnPoint'

// Награда за точку
const rewardsByLevel = {
    1: {salary: 20, exp: 14, jobExp: 12},
    2: {salary: 22, exp: 16, jobExp: 14},
    3: {salary: 24, exp: 18, jobExp: 17},
    4: {salary: 26, exp: 21, jobExp: 20},
    5: {salary: 28, exp: 25, jobExp: 0}
}

// Инициализация модуля
function initialize() {
    mp.events.add({
        'playerEnterVehicle': onEnterBus,
        'playerStartExitVehicle': onExitBus
    })
    createRoutes()
}

// Обработчик входа в автобус
function onEnterBus(player, vehicle, seat) {
    if (!vehicle.isSuch(VehicleTypes.bus)) return
    if (seat === DRIVER_SEAT)
        onDriverEnter(player, vehicle)
    else 
        onPassengerEnter(player, vehicle)
}

// Обработчик выхода из автобуса
function onExitBus(player) {
    if (!(player.vehicle.isSuch(VehicleTypes.bus) && player.seat === DRIVER_SEAT)) return
    player.call(BD_HIDE_ROUTE)
}

// Вход водителя в автобус
function onDriverEnter(player, vehicle) {
    if (!vehicle.canOpen(player, IS_BUS_DRIVER)) return
    showNextPoint(player)
}

// Вход пассажира в автобус
function onPassengerEnter(player, vehicle) {
    const driver = vehicle.getDriver()
    if (!(driver && player.enoughMoney(TICKET_COST))) {
        player.removeFromVehicle()        
        return
    }
    giveBonus(driver)
    player.setMoney(-TICKET_COST)
    Notify.showInfo(player, `${locale('busDriver', 'ticketPay')} ${TICKET_COST}$`)
}

// Создает маршруты движения автобусов
function createRoutes() {
    for (let route in data.routes) {        
        data.routes[route].forEach((position, index) => {
            const point = PointCreator.createShape(position, 3.0)
            point.set(POINT_TYPE, route)
            point.set(POINT_INDEX, index)
            point.onEnter = enterOnPoint
            point.onExit = leaveFromPoint
        })
    }
}

// Прибытие на точку маршрута
function enterOnPoint(player) {
    if (!isPointCorrect(player, this)) return
    player.set(POINT_FLAG, true)
    setTimeout(() => {
        if (player.has(POINT_FLAG)) {
            giveReward(player)
            showNextPoint(player) 
        }
    }, 5000)
}

// Покидание точки маршрута
function leaveFromPoint(player) {
    player.reset(POINT_FLAG)
}

// Показывает следующую точку маршрута
function showNextPoint(player) {
    const route = player.get(POINT_TYPE)
    let index = player.get(POINT_INDEX)
    const points = data.routes[route]
    index = index < points.length - 1 ? index + 1 : 0
    player.set(POINT_INDEX, index)
    player.call(BD_NEXT_POINT, [points[index]])
}

// Выдает награду за точку
function giveReward(player) {
    const reward = rewardsByLevel[player.props.job.level]
    player.setSalary(reward.salary)
    player.setJobExp(reward.jobExp)
    player.setExp(reward.exp)
}

// Выдает награду за перевозку реальных игроков
function giveBonus(player) {
    player.setSalary(TICKET_COST * 3)
    Notify.showSuccess(player, locale('busDriver', 'ticketBonus'))
}

// Проверяет корректность игрока и точки маршрута
function isPointCorrect(player, shape) {
    if (!player.vehicle) return false
    let result = player.vehicle.isSuch(VehicleTypes.bus)
    result &= player.get(POINT_TYPE) === shape.get(POINT_TYPE)
    result &= player.get(POINT_INDEX) === shape.get(POINT_INDEX)
    return result
}

initialize()