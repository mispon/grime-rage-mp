import data from './courier-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { IS_COURIER, COURIER_ON_TARGET } from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY, ONE_KILOMETER } from '../../../../global/constants/VehicleVariables'
import VehicleTypes from '../../../enums/VehicleType'
import VehiclesManager from '../../vehicles/VehiclesManager'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import Misc from '../../../utils/Misc'
import { locale } from '../../../../global/Locale'

/*
** Логика работы курьером
*/
export const TARGET_INDEX = 'DeliveryTargetIndex'
const TIME_OUT = 'DeliveryTimeIsOut'
const SCOOTER_SPEED = 28 // м/с

const rewardsByLevel = {
    1: {salary: 20, exp: 17, jobExp: 12, tip: 7},
    2: {salary: 26, exp: 20, jobExp: 14, tip: 9},
    3: {salary: 34, exp: 23, jobExp: 17, tip: 12},
    4: {salary: 44, exp: 26, jobExp: 20, tip: 13},
    5: {salary: 56, exp: 30, jobExp: 0, tip: 14}
}

// Инициализируер модуль
function initialize() {
    mp.events.add({
        'playerEnterVehicle': onEnterScooter,
        [Events.DELIVERY_TIME_OUT]: player => player.set(TIME_OUT, true),
        [Events.COMPLETE_DELIVERY]: completeDelivery
    })
    createVehicles()
    createTargets()
}

// Обработчик залезания на скутер
function onEnterScooter(player, vehicle) {
    if (!(vehicle.isSuch(VehicleTypes.scooter) && vehicle.canOpen(player, IS_COURIER))) return    
    if (!player.has(TARGET_INDEX)) {
        player.set(TARGET_INDEX, -1)
        setNextTarget(player)
    }    
}

// Устанавливает сл. точку доставки
function setNextTarget(player) {    
    const position = getNextPosition(player)
    const distance = player.dist(position)
    setupTimer(player, distance)
    player.call(Events.COURIER_NEXT, [position])
}

// Возвращает позицию следующей цели
function getNextPosition(player) {
    const currentIndex = player.get(TARGET_INDEX)
    let index
    do {
        index = Misc.randomInt(0, data.targets.length)
    } while(index === currentIndex)
    player.set(TARGET_INDEX, index)
    return data.targets[index].position
}

// Заводит таймер доставки
function setupTimer(player, distance) {
    const kilometers = distance / ONE_KILOMETER
    const deliveryTime = kilometers * 1000 / SCOOTER_SPEED
    const timerData = [
        Events.SET_TIMER,
        parseInt(deliveryTime),
        Events.DELIVERY_TIME_OUT,
        locale('basic', 'tip')
    ]
    player.call(Events.CALL_UI, timerData)   
}

// Обработчик завершения доставки
function completeDelivery(player) {
    if (!player.has(COURIER_ON_TARGET)) return
    // todo: добавить анимацию передачи товара
    player.call(Events.CALL_UI, [Events.RESET_TIMER])
    const reward = rewardsByLevel[player.props.job.level]
    const salary = player.has(TIME_OUT) ? reward.salary : reward.salary + reward.tip    
    player.setSalary(salary)
    player.setJobExp(reward.jobExp)
    player.setExp(reward.exp)
    player.reset(TIME_OUT, COURIER_ON_TARGET)
    setNextTarget(player)
}

// Создает курьерский транспорт
function createVehicles() {
    data.vehicles.forEach(v => {
        setupSpawnInfo(v)
        let vehicle = VehiclesManager.createGeneral(v)
        vehicle.set(GENERAL_VEHICLE_KEY, VehicleTypes.scooter)
    })
}

// Настраивает данные для спавна скутеров
function setupSpawnInfo(info) {
    info.fuelTank = 30
    info.fuelRate = 1.0
    info.mainColor = 38
    info.secondColor = 32
}

// Создает точки доставки
function createTargets() {
    data.targets.forEach((t, index) => {
        const shape = PointCreator.createNpc(t.model, t.position, t.heading)
        shape.set(TARGET_INDEX, index)
        shape.onEnter = comeTo
        shape.onExit = moveAway
    })
}

// Обработчик прибытия игрока к цели доставки
function comeTo(player) {
    if (!(player.has(TARGET_INDEX) && isTargetCorrect(player, this))) return
    player.set(COURIER_ON_TARGET, true)
    Notify.showHint(player, locale('courier', 'toComplete'), locale('courier', 'target'))
}

// Обработчик ухода от цели
function moveAway(player) {
    player.reset(COURIER_ON_TARGET)
    Notify.hideHint(player)
}

// Проверяет корректность точки доставки
function isTargetCorrect(player, shape) {
    return !player.vehicle && player.get(TARGET_INDEX) === shape.get(TARGET_INDEX)
}

initialize()