import data from './airports-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import * as PlayerKey from '../../../../global/constants/PlayerVariables'
import { GENERAL_VEHICLE_KEY } from '../../../../global/constants/VehicleVariables'
import JobType from '../../../enums/JobType'
import DeliveryContractType from '../../../../global/enums/DeliveryContractType'
import VehicleType from '../../../enums/VehicleType'
import { Yellow } from '../../../constants/Colors'
import * as MainPosition from '../../../constants/MainPositions'
import VehiclesManager from '../../vehicles/VehiclesManager'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import Misc from '../../../utils/Misc'
import { applyWorkClothes } from '../JobClothesController'
import { locale } from '../../../../global/Locale'

/*
** Аэропорт
*/
const MIN_LVL = 9
const PLACE_NAME = locale('place', 'airport')
const NAME_KEY = 'NpcName'
const CONTRACTS_KEY = 'NpcContracts'

// Позиции входов и выходов аэропорта
const enterPosition = MainPosition.AirPort
const afterEnterPosition = new mp.Vector3(-1563.37, -3204.75, 13.94)
const exitPositions = [
    new mp.Vector3(-1583.99, -3181.08, 14.65),
    new mp.Vector3(-1522.60, -3216.56, 14.65)
]
const afterExitPosition = new mp.Vector3(-1039.13, -2739.75, 20.17)

const contracts = [
    {type: DeliveryContractType.MechanicalDrawings, reward: 400, target: {x: 1705.22, y: 3254.04, z: 39.01}},
    {type: DeliveryContractType.CollectedPrototypes, reward: 400, target: {x: -1579.52, y: -2808.01, z: 11.45}},
    {type: DeliveryContractType.Fertilizer, reward: 500, target: {x: 2103.87, y: 4793.69, z: 39.34}},
    {type: DeliveryContractType.Crop, reward: 500, target: {x: -1579.52, y: -2808.01, z: 11.45}},
    {type: DeliveryContractType.MilitaryEquipment, reward: 600, target: {x: -2014.16, y: 2862.59, z: 31.24}},
    {type: DeliveryContractType.MilitaryReports, reward: 600, target: {x: -1579.52, y: -2808.01, z: 11.45}}
]

// Инициализация модуля
function initialize() {
    mp.events.add({
        [Events.PILOT_WORK]: startWork,
        [Events.PILOT_SALARY]: getSalary,
        [Events.PILOT_SELECT_CONTRACT]: onContractSelected
    })
    createAirportEnters()
    createVehicles()
    createNpcs()
    setInterval(() => {
        contracts.forEach(c => c.reward = Misc.randomInt(350, 750))
    }, 600000)
}

// Начало работы
function startWork(player) {
    if (!player.enoughLevel(MIN_LVL)) return
    if (player.startWorkAs(JobType.pilot)) {
        player.set(PlayerKey.IS_PILOT, true)
        applyWorkClothes(player)
        Notify.showInfo(player, locale('pilot', 'toStart'))
    }
}

// Получение зарплаты
function getSalary(player) {
    if (player.workAs(JobType.pilot)) {
        player.reset(
            PlayerKey.IS_PILOT,
            PlayerKey.DELIVERY_CONTRACT,
            PlayerKey.PILOT_ON_TARGET
        )
        player.call(Events.PILOT_HIDE_TARGET)
        player.finishWork()
    }
    player.applySalary(JobType.pilot)
}

// Обработчик выбора контракта
function onContractSelected(player, contract) {
    if (!player.workAs(JobType.pilot)) {
        Notify.showError(player, locale('pilot', 'needBePilot'))
        return
    }
    if (player.has(PlayerKey.DELIVERY_CONTRACT)) {
        Notify.showError(player, locale('trucker', 'alreadyHasContract'))
        return
    }
    player.set(PlayerKey.DELIVERY_CONTRACT, contract)
    Notify.showInfo(player, locale('trucker', 'takedContract'))
}

// Создает входы и выходы аэропорта
function createAirportEnters() {
    const markerRotation = new mp.Vector3(0, -180, 0)
    const enter = PointCreator.createMarker(21, enterPosition, Yellow, markerRotation)
    enter.onEnter = player => player.position = afterEnterPosition
    exitPositions.forEach(position => {
        const exit = PointCreator.createMarker(21, position, Yellow, markerRotation)
        exit.onEnter = player => player.position = afterExitPosition
    })
}

// Создает самолёты
function createVehicles() {
    data.planes.forEach(p => {
        setupSpawnInfo(p)
        const vehicle = VehiclesManager.createGeneral(p)
        vehicle.set(GENERAL_VEHICLE_KEY, VehicleType.plane)
    })
}

// Настраивает данные для спавна самолётов
function setupSpawnInfo(info) {
    info.numberPlate = 'Plane'
    info.fuelTank = 0
    info.fuelRate = 0
    info.mainColor = 111
    info.secondColor = 74
}

// Создает нпс лётчиков
function createNpcs() {
    PointCreator.createBlip(MainPosition.AirPort, 359, 63, PLACE_NAME)
    data.npcs.forEach(e => {
        PointCreator.createBlip(e.position, 572, 53, PLACE_NAME)
        const name = locale('npc', e.name)
        const shape = PointCreator.createNpc(e.model, e.position, e.heading, name)
        shape.set(NAME_KEY, name)
        shape.set(CONTRACTS_KEY, e.contracts)
        shape.onEnter = comeToNpc
        shape.onExit = moveAwayFromNpc
    })
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), this.get(NAME_KEY))
    player.set(PlayerKey.NPC_MENU, true)
    const contractsIndexes = this.get(CONTRACTS_KEY)
    const data = {
        contracts: contracts.filter((c, i) => contractsIndexes.includes(i)),
        selectCallback: Events.PILOT_SELECT_CONTRACT
    }
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'PilotMenu', data})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(PlayerKey.NPC_MENU)
}

initialize()