import * as Events from '../../../../global/constants/ServerEvents'
import { IS_PILOT, DELIVERY_CONTRACT } from '../../../../global/constants/PlayerVariables'
import VehicleTypes from '../../../enums/VehicleType'
import Notify from '../../../utils/Notify'
import { locale } from '../../../../global/Locale'

/*
** Серверная логика работы пилотом
*/

const rewardByLevel = {
    1: {salary: 30, exp: 34, jobExp: 14},
    2: {salary: 40, exp: 38, jobExp: 15},
    3: {salary: 60, exp: 42, jobExp: 16},
    4: {salary: 80, exp: 46, jobExp: 17},
    5: {salary: 120, exp: 50, jobExp: 0}
}

// Инициализация модуля
function initialize() {
    mp.events.add({
        'playerEnterVehicle': onEnterVehicle,
        'playerStartExitVehicle': onExitFromVehicle,
        [Events.PILOT_COMPLETE_ORDER]: completeDelivery
    })
}

// Обработчик входа в самолет
function onEnterVehicle(player, vehicle) {
    if (!(
        vehicle.isSuch(VehicleTypes.plane) &&
        vehicle.canOpen(player, IS_PILOT) &&
        jobRankAllow(player, vehicle))
    ) return
    let contract = player.get(DELIVERY_CONTRACT)
    if (!contract) {
        Notify.showWarn(player, locale('player', 'noContract'))
        return
    }
    contract = JSON.parse(contract)
    player.call(Events.PILOT_SHOW_TARGET, [contract.target])
}

// Обработчик выхода из самолета
function onExitFromVehicle(player) {
    if (!player.vehicle.isSuch(VehicleTypes.plane)) return
    player.call(Events.PILOT_HIDE_TARGET)
}

// Обработчик завершения доставки
function completeDelivery(player) { 
    const contract = JSON.parse(player.get(DELIVERY_CONTRACT))
    const reward = rewardByLevel[player.props.job.level]
    player.setSalary(contract.reward + reward.salary)
    player.setJobExp(reward.jobExp)
    player.setExp(reward.exp)
    player.reset(DELIVERY_CONTRACT)
}

// Определяет, позволяет ли ранг работы игрока использовать выбранный самолет
function jobRankAllow(player, vehicle) {
    let result = true
    const jobLevel = player.props.job.level
    switch(vehicle.props.model) {
        case 'nimbus':
            result = jobLevel > 4
            break
        case 'vestra':
            result = jobLevel > 3
            break
        case 'seabreeze':
            result = jobLevel > 2
            break
        case 'mammatus':
            result = jobLevel > 1
            break
    }
    if (!result) {
        Notify.showError(player, locale('player', 'lowJobRank'))
        player.removeFromVehicle()
    }
    return result
}

initialize()