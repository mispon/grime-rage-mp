import * as Events from '../../../../global/constants/ServerEvents'
import { IS_TRUCKER, DELIVERY_CONTRACT, TRUCKER_ON_TARGET } from '../../../../global/constants/PlayerVariables'
import VehicleTypes from '../../../enums/VehicleType'
import Notify from '../../../utils/Notify'
import { locale } from '../../../../global/Locale'

/*
** Серверная логика работы дальнобойщиком
*/
const rewardByLevel = {
    1: {salary: 20, exp: 32, jobExp: 22},
    2: {salary: 40, exp: 34, jobExp: 23},
    3: {salary: 60, exp: 36, jobExp: 24},
    4: {salary: 80, exp: 38, jobExp: 25},
    5: {salary: 100, exp: 40, jobExp: 0}
}

// Инициализация модуля
function initialize() {
    mp.events.add({
        'playerEnterVehicle': onEnterVehicle,
        'trailerAttached': onTrailerChange,
        [Events.TRUCKER_ON_TARGET]: onTargetEvent
    })
}

// Обработчик входа в транспорт
function onEnterVehicle(player, vehicle) {
    if (!(vehicle.isSuch(VehicleTypes.truck) && vehicle.canOpen(player, IS_TRUCKER))) return
    Notify.showHint(player, locale('trucker', 'trailerControl'))
}

// Обработчик изменения состояния прицепа
function onTrailerChange(vehicle, trailer) {
    if (!vehicle.isSuch(VehicleTypes.truck)) return
    if (trailer)
        trailerAttached(vehicle, trailer)
    else
        trailerDetached(vehicle)
}

// Обработчик присоединения прицепа
function trailerAttached(vehicle, trailer) {    
    vehicle.props.trailer = trailer
    trailer.props.afk = null
    const player = vehicle.getDriver()
    if (!player.has(DELIVERY_CONTRACT)) {
        Notify.showWarn(player, locale('player', 'noContract'))
        return
    }
    const contract = JSON.parse(player.get(DELIVERY_CONTRACT))
    player.call(Events.TRUCKER_SHOW_TARGET, [contract.target])
}

// Обработчик отсоединения прицепа
function trailerDetached(vehicle) {
    vehicle.props.trailer.props.afk = Date.now()
    const player = vehicle.getDriver()
    if (player.has(TRUCKER_ON_TARGET)) {
        completeDelivery(player)
    }
    player.vehicle.props.trailer = null
    player.call(Events.TRUCKER_HIDE_TARGET)
}

// Обработчик завершения доставки
function completeDelivery(player) {
    player.vehicle.props.trailer.restore()    
    const contract = JSON.parse(player.get(DELIVERY_CONTRACT))
    const reward = rewardByLevel[player.props.job.level]
    player.setSalary(contract.reward + reward.salary)
    player.setJobExp(reward.jobExp)
    player.setExp(reward.exp)
    player.reset(DELIVERY_CONTRACT, TRUCKER_ON_TARGET)
}

// Обработчик событий целевой точки
function onTargetEvent(player, isArrived) {
    if (isArrived)
        player.set(TRUCKER_ON_TARGET, true)
    else
        player.reset(TRUCKER_ON_TARGET)
}

initialize()