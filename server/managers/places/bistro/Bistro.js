import data from './bistro-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { NPC_MENU, IS_COURIER } from '../../../../global/constants/PlayerVariables'
import { TARGET_INDEX } from '../../jobs/courier/CourierManager'
import * as MainPosition from '../../../constants/MainPositions'
import JobType from '../../../enums/JobType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import { applyWorkClothes } from '../../jobs/JobClothesController'
import { locale } from '../../../../global/Locale'

const PLACE_NAME = locale('place', 'bistro')
const NPC_NAME = locale('npc', 'bistro')

/*
** Закусочная
*/

// Инициализация модуля
function initialize() {
    mp.events.add({
        [Events.BISTRO_ORDER]: processOrder,
        [Events.COURIER_WORK]: startWork,
        [Events.COURIER_SALARY]: getSalary,
    })
    createNpc()
}

// Обработчик заказа в бистро
function processOrder(player, order) {
    order = JSON.parse(order)
    if (!player.enoughMoney(order.price)) return
    addOrderValue(player, 'satiety', order.satiety)
    addOrderValue(player, 'thirst', order.thirst)
    player.setMoney(-order.price)
    player.call(Events.CALL_UI, [Events.BISTRO_HIDE])
}

// Пополняет характеристику игрока
function addOrderValue(player, propName, value) {
    player.props[propName] += value
    if (player.props[propName] > 100) {
        player.props[propName] = 100
    }
}

// Устроиться курьером
function startWork(player) {
    if (!player.startWorkAs(JobType.courier)) return       
    player.set(IS_COURIER, true)
    applyWorkClothes(player)
    Notify.showInfo(player, locale('courier', 'toStart'))
}

// Получить зарплату
function getSalary(player) {
    if (player.workAs(JobType.courier)) {
        player.reset(IS_COURIER, TARGET_INDEX)
        player.call(Events.COURIER_HIDE)
        player.call(Events.CALL_UI, [Events.RESET_TIMER])
        player.finishWork()
    }    
    player.applySalary(JobType.courier)    
}

// Создает нпс закусочной
function createNpc() {
    const npc = data.npc
    PointCreator.createBlip(MainPosition.Bistro, 106, 49, PLACE_NAME)
    const shape = PointCreator.createNpc(npc.model, MainPosition.Bistro, npc.heading, NPC_NAME)
    shape.onEnter = comeToNpc
    shape.onExit = moveAwayFromNpc
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'BistroMenu', data: {}})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(NPC_MENU)
}

initialize()