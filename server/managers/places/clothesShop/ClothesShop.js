import data from './clothes-shops-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { NPC_MENU, LAST_POSITION, LAST_DIMENSION } from '../../../../global/constants/PlayerVariables'
import ClothesShopType from '../../../enums/ClothesShopType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import { locale } from '../../../../global/Locale'

const NPC_NAME = locale('npc', 'shop')
const SHOP_TYPE = 'ShopTypeKey'
const DRESSING_DATA = 'DressingDataKey'

/*
** Серверная логика магазинов одежды
*/

// Инициализация модуля
function initialize() {
    mp.events.add({
        [Events.CS_ENTER_DR]: enterDressingRoom,
        [Events.CS_LEAVE_DR]: leaveDressingRoom,
        [Events.CS_BUY]: buyClothes,
        [Events.CS_RESET_CLOTHES]: resetClothes
    })
    createShops()
}

// Вход в примерочную
function enterDressingRoom(player) {
    const data = player.get(DRESSING_DATA)
    player.set(LAST_POSITION, JSON.stringify(player.position))
    player.set(LAST_DIMENSION, player.dimension)
    player.position = data.player.position
    player.heading = data.player.heading
    player.dimension = player.props.id
    const info = {shopType: player.get(SHOP_TYPE), isMale: player.isMale(), clothes: player.props.clothes}
    player.call(Events.CS_ENTER_DR, [JSON.stringify(data.camera), JSON.stringify(info), player.heading])
}

// Выход из примерочной
function leaveDressingRoom(player) {
    player.position = JSON.parse(player.get(LAST_POSITION))
    player.dimension = player.get(LAST_DIMENSION)
}

// Обработчик покупки одежды
function buyClothes(player, item, hasItem) {
    item = JSON.parse(item)

    if (item.drawable === -1 && item.isProp)
        return player.resetAllProps(item.slot)

    if (!(hasItem || player.enoughMoney(item.price)))
        return player.call(Events.CALL_UI, [Events.CS_NO_MONEY])

    if (hasItem)
        return player.dressOn(item)

    player.setMoney(-item.price)
    item.textures = [item.texture]
    player.addClothes(item)
    player.call(Events.CALL_UI, [Events.CS_CONFIRM_PURCHASE])
}

// Применяет активную одежду игрока
function resetClothes(player) {
    player.applyCurrentClothes()
}

// Создает магазины
function createShops() {
    data.forEach(e => {
        const isSubUrban = e.type === ClothesShopType.SubUrban
        const blipColor = isSubUrban ? 25 : 63
        const placeName = isSubUrban ? 'Sub Urbar' : 'Ponsonbys'
        PointCreator.createBlip(e.position, 73, blipColor, placeName)
        const shape = PointCreator.createNpc(e.seller.model, e.seller.position, e.seller.heading, NPC_NAME)
        shape.onEnter = player => comeToNpc(player, e)
        shape.onExit = moveAwayFromNpc
    })
}

// Игрок подошел к нпс
function comeToNpc(player, shop) {
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    player.set(SHOP_TYPE, shop.type)
    player.set(DRESSING_DATA, shop.dressingRoom)
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'ClothesShopMenu', data: {}})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    Notify.hideHint(player)
    player.reset(NPC_MENU, SHOP_TYPE, DRESSING_DATA)
}

initialize()