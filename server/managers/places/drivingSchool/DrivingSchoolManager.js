import data from './driving-school-data.json'
import {
    START_PRACTICE_EXAM,
    NEXT_EXAM_POINT,
    HIDE_EXAM_POINT,
    DS_OVER_SPEED    
} from '../../../../global/constants/ServerEvents'
import { DRIVING_EXAM, VEHICLE_DAMAGES, VEHICLE_OVER_SPEED } from '../../../../global/constants/PlayerVariables'
import moment from 'moment'
import VehicleTypes from '../../../enums/VehicleType'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import DateHelper from '../../../utils/DateHelper'
import { locale } from '../../../../global/Locale'

/*
** Логика сдачи экзаменов в автошколе
*/

const SHAPE_KEY = 'DrivingExamPoint'

// Инициализация мэнеджера автошколы
function initialize() {
    mp.events.add({
        'playerEnterVehicle': onEnterVehicle,
        'vehicleDamage': onVehicleDamaged,
        [DS_OVER_SPEED]: onOverSpeed
    })
    createPoints()
}

// Обработчик входа в транспорт
function onEnterVehicle(player, vehicle) {
    if (!vehicle.isSuch(VehicleTypes.drivingSchool)) return
    if (player.get(DRIVING_EXAM) != vehicle.get(DRIVING_EXAM)) {
        Notify.showError(player, locale('player', 'unavailableVehicle'))
        player.removeFromVehicle()
        vehicle.setDriver(null)
        return
    }
    if (!player.has(SHAPE_KEY)) 
        startExam(player)
}

// Обработчик повреждений транспорта
function onVehicleDamaged(vehicle) {
    if (!vehicle.isSuch(VehicleTypes.drivingSchool)) return
    const driver = vehicle.getDriver()
    let count = driver.get(VEHICLE_DAMAGES)
    Notify.showWarn(driver, `${getMessage('vehicleDamages')} ${++count} / 3`)
    if (count < 3)
        driver.set(VEHICLE_DAMAGES, count)
    else
        finishExam(driver, false)
}

// Обработчик превышения скорости
function onOverSpeed(player) {
    let count = player.get(VEHICLE_OVER_SPEED)
    Notify.showWarn(player, `${getMessage('overSpeed')} ${++count} / 5`)
    if (count < 5)
        player.set(VEHICLE_OVER_SPEED, count)
    else
        finishExam(player, false)
}

// Обработчик начала экзамена
function startExam(player) {
    player.set(SHAPE_KEY, 0)
    player.set(VEHICLE_DAMAGES, 0)
    player.set(VEHICLE_OVER_SPEED, 0)
    Notify.showWarn(player, getMessage('practiceWarn'))
    player.call(START_PRACTICE_EXAM)
    player.call(NEXT_EXAM_POINT, [data.points[0].position])
}

// Обработчик завершения экзамена
function finishExam(player, result) {
    if (result) {
        const license = player.get(DRIVING_EXAM)
        player.props.driver.licenses.push(license)
        Notify.showSuccess(player, locale('player', 'newLicense'))
    }
    else {
        let nextTryTime = moment().add(1, 'hours')
        player.props.driver.timeToNextTry = nextTryTime
        Notify.showError(player, `${getMessage('fail')} ${DateHelper.format(nextTryTime)}`, 10000)
    }
    player.reset(DRIVING_EXAM, VEHICLE_DAMAGES, SHAPE_KEY, VEHICLE_OVER_SPEED)
    setTimeout(() => player.removeFromVehicle(), 2000)
    player.call(HIDE_EXAM_POINT)
}

// Показывает следующую точку маршрута
function showNextPoint(player) {
    const pointIndex = this.get(SHAPE_KEY)
    if (!isPointCorrect(player, pointIndex)) return
    const nextPointIndex = pointIndex + 1
    if (nextPointIndex < data.points.length) {
        player.set(SHAPE_KEY, nextPointIndex)
        player.call(NEXT_EXAM_POINT, [data.points[nextPointIndex].position])
    }
    else
        finishExam(player, true)
}

// Создает точки маршрута экзамена
function createPoints() {
    data.points.forEach((point, index) => {
        let shape = PointCreator.createShape(point.position, 3)
        shape.set(SHAPE_KEY, index)
        shape.onEnter = showNextPoint
    })
}

// Проверяет условия корректности точки
function isPointCorrect(player, pointIndex) {
    let result = player.vehicle && player.vehicle.isSuch(VehicleTypes.drivingSchool)
    result &= player.has(DRIVING_EXAM)
    result &= pointIndex === player.get(SHAPE_KEY)
    return result
}

// Возвращает сообщение автошколы
function getMessage(value) {
    return locale('drivingSchool', value)
}

initialize()