import data from './fillings-data.json'
import * as Events from '../../../../global/constants/ServerEvents'
import { NPC_MENU } from '../../../../global/constants/PlayerVariables'
import { VividCyan } from '../../../constants/Colors'
import PointCreator from '../../../utils/PointCreator'
import Notify from '../../../utils/Notify'
import { locale } from '../../../../global/Locale'

const POINT_KEY = 'PatrolPoint'
const PLACE_NAME = locale('place', 'filling')
const NPC_NAME = locale('npc', 'shop')

/*
** Серверная логика заправки
*/

// Инициализация модуля
function initialize() {
    mp.events.add({
        [Events.FILL_VEHICLE]: fillVehicle,
        [Events.FILL_CANISTER]: fillCanister,
        [Events.FS_SHOP_BUY]: buyShopItem
    })
    createStations()
}

// Обработчик заправки транспорта
function fillVehicle(player, liters, price) {
    if (!player.enoughMoney(price)) return
    const vehicle = findVehicle(player)
    if (vehicle && vehicle.fill) {
        vehicle.fill(liters)
    }
    Notify.showInfo(player, locale('vehicle', 'gotFuel'))
}

// Обработчик заправки канистры
function fillCanister(player, liters, price) {
    // todo
}

// Обработчик покупки в магазине заправки
function buyShopItem(player, itemData) {
    // todo
}

// Создает заправки
function createStations() {
    data.forEach(f => {
        PointCreator.createBlip(f.position, 361, 12, PLACE_NAME)
        const shape = PointCreator.createNpc("cs_guadalope", f.npc.position, f.npc.heading, NPC_NAME)
        shape.onEnter = comeToNpc
        shape.onExit = moveAwayFromNpc
        f.points.forEach(p => {
            const point = PointCreator.createMarker(23, p, VividCyan, new mp.Vector3(), 1.7, 3)
            point.onEnter = player => comeToPatrol(player, f.id)
            point.onExit = moveAwayFromPatrol
        })
    })
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    let data = {}
    if (player.has(POINT_KEY)) {
        const vehicle = findVehicle(player)
        data = {fuel: vehicle.props.fuel, fuelTank: vehicle.props.fuelTank}
    }
    player.call(Events.SETUP_MENU, [JSON.stringify({name: 'FillingsMenu', data})])
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(NPC_MENU)
}

// Игрок подъехал к колонке
function comeToPatrol(player, stationId) {
    if (!player.vehicle) return
    player.set(POINT_KEY, {id: stationId, vehicleId: player.vehicle.id})
}

// Игрок отъехал от колонки
function moveAwayFromPatrol(player) {
    if (!player.vehicle) return
    player.reset(POINT_KEY)
}

// Находит транспорт в пуле по идентификатору
function findVehicle(player) {
    const id = player.get(POINT_KEY).vehicleId
    return mp.vehicles.at(id)
}

initialize()