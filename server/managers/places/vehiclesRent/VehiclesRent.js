import data from './vehicles-rent-data.json'
import { SETUP_MENU, RENT_VEHICLE } from '../../../../global/constants/ServerEvents'
import { NPC_MENU } from '../../../../global/constants/PlayerVariables'
import VehiclesManager from '../../vehicles/VehiclesManager'
import Notify from '../../../utils/Notify'
import PointCreator from '../../../utils/PointCreator'
import Misc from '../../../utils/Misc'
import { locale } from '../../../../global/Locale'

/*
** Логика аренды транспорта
*/
const PLACE_NAME = locale('place', 'vehiclesRent')
const NPC_NAME = locale('npc', 'vehiclesRent')
const POINT_INDEX = 'RentPointIndex'
const RENT_TYPE = 'SelectedRentType'
const RENT_ACCEPT = 'RentAccept'

// Инициализация модуля
function initialize() {
    mp.events.add({
        [RENT_VEHICLE]: rentVehicle,
        [RENT_ACCEPT]: rentAccept
    })
    createPoints()
}

// Обработчик выбора транспорта
function rentVehicle(player, type) {
    player.set(RENT_TYPE, type)
    const price = data.vehicles[type].price
    const header = locale('vehiclesRent', 'rentHeader')
    const desc = `${locale('vehiclesRent', 'rentDesc')} ${price}$`
    Notify.showDialog(player, header, desc, RENT_ACCEPT)
}

// Обработчик подтверждения оплаты аренды
function rentAccept(player) {
    let vehicleInfo = data.vehicles[player.get(RENT_TYPE)]
    if (!player.enoughMoney(vehicleInfo.price)) return
    player.setMoney(-vehicleInfo.price)
    vehicleInfo.ownerId = player.props.id
    setupSpawnInfo(vehicleInfo, player.get(POINT_INDEX))    
    const vehicle = VehiclesManager.createGeneral(vehicleInfo)
    vehicle.props.afk = Date.now()
    vehicle.isRented = true
}

// Подготавливает информацию о транспорте для спавна
function setupSpawnInfo(info, pointIndex) {
    let point = data.points[pointIndex]
    info.numberPlate = 'Rent'
    info.position = point.spawnPosition
    info.heading = point.spawnHeading
    info.mainColor = Misc.randomInt(0, 159)
    info.secondColor = Misc.randomInt(0, 159)
}

// Создает точки аренды
function createPoints() {
    data.points.forEach((point, index) => {
        PointCreator.createBlip(point.position, 512, 9, PLACE_NAME)
        let shape = PointCreator.createNpc('g_m_m_chigoon_01', point.position, point.heading, NPC_NAME)
        shape.set(POINT_INDEX, index)
        shape.onEnter = comeToNpc
        shape.onExit = moveAwayFromNpc
    })
}

// Игрок подошел к нпс
function comeToNpc(player) {
    if (player.vehicle) return
    Notify.showHint(player, locale('hint', 'npcTalk'), NPC_NAME)
    player.set(NPC_MENU, true)
    player.set(POINT_INDEX, this.get(POINT_INDEX))
    player.call(SETUP_MENU, [JSON.stringify({name: 'VehiclesRent', data: {}})])    
}

// Игрок отошел от нпс
function moveAwayFromNpc(player) {
    if (player.vehicle) return
    Notify.hideHint(player)
    player.reset(NPC_MENU, POINT_INDEX, RENT_TYPE)
}

initialize()