
/*
** Логика управления внешним видом игрока
*/
class AppearanceManager {
    constructor() {
        if (!AppearanceManager.instance) {
            AppearanceManager.instance = this
        }
        return AppearanceManager.instance
    }
}

const instance = new AppearanceManager()

export default instance