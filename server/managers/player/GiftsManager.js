import { CALL_UI, GIVE_GIFT, SET_GIFT_TIMER } from '../../../global/constants/ServerEvents'
import Notify from '../../utils/Notify'
import { locale } from '../../../global/Locale'

/*
** Логика получения подарков за нахождение онлайн
*/
const rewardByState = {1: 10, 2: 40, 3: 200}

mp.events.add(GIVE_GIFT, givePlayersGift)

// Обработчик выдачи подарка игроку
function givePlayersGift(player, state) {
    const reward = player.props.level * rewardByState[state]
    player.setMoney(reward)
    Notify.showInfo(player, locale('player', 'gift'))
    player.call(CALL_UI, [SET_GIFT_TIMER])
}

// Выдает награду, за вход в игру несколько дней подряд
export const giveDaysGift = player => {

}