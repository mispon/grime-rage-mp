import VectorHelper from '../../utils/VectorHelper'

const SATIETY_CONSUMPTION = 0.003;
const PASSIVE_CONSUMPTION = 0.07

/*
** Вспомогательная логика, связанная с игроками
*/
class PlayerManager {
    constructor() {
        if (!PlayerManager.instance) {
            PlayerManager.instance = this
            this.syncTimer = setInterval(() => {
                try {
                    this.syncPlayers()
                } catch (e) {console.log(e)}
            }, 10000)
        }
    }
    
    // Синхронизирует состояние игроков
    syncPlayers() {
        mp.players.forEach(player => {
            if (!(player.logged && player.props.position)) return
            const dist = VectorHelper.distance(player.position, player.props.position)
            updateStats(player, dist)
            player.props.position = player.position
            player.updateHUD()
        })
    }
}

// Обновляет значения сытости и жажды
const updateStats = (player, dist) => {
    applyHealthDecrease(player)
    applyConsumption(player, PASSIVE_CONSUMPTION)
    if (!player.vehicle && dist < 75) {
        const consumption = (dist / 0.5) * SATIETY_CONSUMPTION
        applyConsumption(player, consumption)
    }
}

// Применяет расход здоровья
const applyHealthDecrease = player => {
    let healthDecrease = 2
    healthDecrease -= player.props.satiety > 0 ? 1 : 0
    healthDecrease -= player.props.thirst > 0 ? 1 : 0
    player.health -= healthDecrease
}

// Применяет значение расхода сытости и жажды
const applyConsumption = (player, cons) => {
    player.props.satiety -= cons
    player.props.thirst -= cons * 1.5
}

const instance = new PlayerManager()

export default instance