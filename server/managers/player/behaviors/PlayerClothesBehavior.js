/*
** Поведение, связанное с одеждой игрока
*/
class PlayerClothesBehavior {
    constructor(player) {        
        player.dressOn = this.dressOn
        player.addClothes = this.addClothes
        player.applyCurrentClothes = this.applyCurrentClothes
        player.applyTempClothes = this.applyTempClothes
        player.giveDefaultClothes = this.giveDefaultClothes
        player.resetAllProps = this.resetAllProps
    }

    // Добавляет новый элемент одежды
    addClothes(item) {
        let existingItem = findExistingItem(this, item)
        if (existingItem) {
            existingItem.texture = item.texture
            existingItem.textures.push(item.texture)
            this.dressOn(existingItem)
        }
        else {
            this.props.clothes.push(item)
            this.dressOn(item)
        }
    }

    // Одевает элемент одежды
    dressOn(item) {
        if (item._id === undefined) item = findExistingItem(this, item)
        const current = getCurrentItem(this, item.slot, item.isProp)
        if (current !== undefined) current.dressed = false
        item.dressed = true
        applyClothes(this, item)
    }

    // Одевает текущую одежду, выбранную игроком
    applyCurrentClothes() {
        const clothes = this.props.clothes.filter(e => e.dressed)
        clothes.forEach(e => applyClothes(this, e))
    }

    // Одевает все переданные вещи на игрока
    // Используется для рабочей одежды, эвентов и тд
    applyTempClothes(clothes) {
        clothes.forEach(e => applyClothes(this, e))
    }

    // Сбрасывает все аксессуары
    resetAllProps(slot) {
        this.props.clothes.filter(e => e.slot === slot && e.isProp).forEach(e => e.dressed = false)
    }

    // Одевает начальную одежду
    giveDefaultClothes() {
        const isMale = this.isMale()
        const clothes = [
            {slot: 0, drawable: isMale ? 11 : 120, texture: 0, textures: [0], isProp: true},
            {slot: 1, drawable: isMale ? 0 : 12, texture: 0, textures: [0], isProp: true},
            {slot: 11, drawable: 0, torso: 0, undershirt: isMale ? 57 : 2, texture: 0, textures: [0]},
            {slot: 4, drawable: 0, texture: 0, textures: [0]},
            {slot: 6, drawable: isMale ? 1 : 3, texture: 0, textures: [0]},
            {slot: 5, drawable: 0, texture: 0, textures: [0]}
        ]
        clothes.forEach(e => {
            this.props.clothes.push(e)
            this.dressOn(e)
        })
    }
}

// Применяет переданный элемент одежды на игрока
function applyClothes(player, item) {
    if (item.torso !== undefined) player.setClothes(3, item.torso, 0, 0)
    if (item.undershirt !== undefined) player.setClothes(8, item.undershirt, 0, 0)
    if (item.isProp)
        player.setProp(item.slot, item.drawable, item.texture)
    else
        player.setClothes(item.slot, item.drawable, item.texture, 0)
}

// Находит текущий одетый элемент на данном слоте
function getCurrentItem(player, slot, isProp) {
    return player.props.clothes.find(e => e.slot === slot && e.dressed && e.isProp === isProp)
}

// Находит существующий элемент одежды
function findExistingItem(player, item) {
    return player.props.clothes.find(e => e.slot === item.slot && e.drawable === item.drawable)
}

export default PlayerClothesBehavior