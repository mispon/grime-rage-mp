import Notify from '../../../utils/Notify'
import JobType from '../../../enums/JobType'
import { locale } from '../../../../global/Locale'

// Уровень работы и кол-во опыта для перехода на сл.
const expByLevel = {
    1: 250,
    2: 550,
    3: 750,
    4: 1200
}
const MAX_LEVEL = 5

/*
** Поведение, связанное с работами игрока
*/
class PlayerJobBehavior {
    constructor(player) {
        player.startWorkAs = this.startWorkAs
        player.finishWork = this.finishWork
        player.applySalary = this.applySalary
        player.setSalary = this.setSalary
        player.setJobExp = this.setJobExp
        player.workAs = this.workAs
        player.hasJob = this.hasJob
    }

    // Обработчик начала работы
    startWorkAs(type) {
        if (this.hasJob()) return false
        this.props.job = getOrCreateJob(this, type)
        const jobName = getJobName(type)
        Notify.showInfo(this, `${locale('player', 'startWorkAs')} ${jobName}`)
        return true
    }

    // Обработчик завершения работы
    finishWork() {        
        this.props.job = null
        Notify.showInfo(this, locale('player', 'finishWork'))
        this.applyCurrentClothes()
    }

    // Выдает зарплату
    applySalary(type) {
        const job = getOrCreateJob(this, type)
        if (job === undefined) {
            console.log(`[ERROR]: Работа по типу ${type} не найдена и не создана!`)
            return
        }
        if (job.salary === 0) return
        this.setMoney(job.salary)
        Notify.showInfo(this, `${locale('player', 'salary')} ${job.salary}$`)
        job.salary = 0
    }

    // Добавляет зарплату или штраф
    setSalary(amount) {
        if (this.isPremium() && amount > 0)
            amount += Math.ceil(amount * 0.5)
        this.props.job.salary += amount
        if (amount > 0)
            Notify.showInfo(this, `${locale('player', 'salary')} ${amount}$`)
        else
            Notify.showWarn(this, `${locale('player', 'penalty')} ${amount}$`)
    }

    // Добавляет очки опыта
    setJobExp(amount) {
        const job = this.props.job
        if (job.level === MAX_LEVEL) return
        if (this.isPremium()) 
            amount += Math.ceil(amount * 0.2)
        const expToLevelUp = expByLevel[job.level]
        let newExp = job.exp + amount
        if (newExp >= expToLevelUp) {
            job.level += 1
            job.exp += newExp - expToLevelUp
            Notify.showSuccess(this, `${locale('player', 'jobLevelUp')} ${job.level}!`)
        }
        else
            job.exp = newExp
    }

    // Проверяет, что игрок работает на данной работе
    workAs(type) {
        const job = this.props.job
        return job && job.type === type
    }

    // Проверяет, работает ли игрок
    hasJob() {
        if (this.props.job) {
            const jobName = getJobName(this.props.job.type)
            Notify.showError(this, `${locale('player', 'alreadyWork')} ${jobName}`)
            return true
        }
        return false
    }
}

// Возвращает работу по типу или создает новую при необходимости
function getOrCreateJob(player, type) {
    const jobs = player.props.jobs
    if (!jobs.some(j => j.type === type)) {
        jobs.push({type, level: 1, exp: 0, salary: 0})
    }
    return jobs.find(j => j.type === type)
}

// Возвращает локализованное название работы
function getJobName(type) {
    return locale('jobs', getJobValue(type))
}

// Возвращает значение названия работы
function getJobValue(type) {
    switch(type) {
        case JobType.business: return 'business'
        case JobType.courier: return 'courier'
        case JobType.busDriver: return 'busDriver'
        case JobType.taxi: return 'taxi'
        case JobType.trucker: return 'trucker'
        case JobType.pilot: return 'pilot'
    }
}

export default PlayerJobBehavior