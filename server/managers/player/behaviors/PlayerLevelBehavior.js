import Notify from '../../../utils/Notify'
import { locale } from '../../../../global/Locale'

const MAX_LEVEL = 20
const expByLevel = {
    1: 100,
    2: 150,
    3: 225,
    4: 350,
    5: 550,
    6: 830,
    7: 1250,
    8: 1900,
    9: 2850,
    10: 4300,
    11: 5160,
    12: 6192,
    13: 7430,
    14: 8916,
    15: 10699,
    16: 12838,
    17: 15405,
    18: 18486,
    19: 22183,
    20: 26619
}

/*
** Логика опыта игрока
*/
class PlayerLevelPehavior {
    constructor(player) {
        player.setExp = this.setExp
        player.enoughLevel = this.enoughLevel
        player.getExpPercentage = this.getExpPercentage
    }

    // Добавляет очки опыта
    setExp(amount) {
        if (this.props.level === MAX_LEVEL || amount === 0) return
        if (this.isPremium()) amount += Math.floor(amount * 0.5)
        const expToNext = expByLevel[this.props.level]
        let newExp = this.props.exp + amount
        if (newExp >= expToNext) {
            this.props.level += 1
            this.props.exp = newExp - expToNext
            Notify.showSuccess(this, locale('player', 'levelUp'))
        }
        else
            this.props.exp += amount
        this.updateHUD()
    }

    // Проверяет, достаточен ли уровень
    enoughLevel(value) {
        if (this.props.level < value) {
            Notify.showError(this, `${locale('player', 'lowLevel')} ${value}`)
            return false
        }
        return true
    }

    // Возвращает количество опыта текущего уровня
    getExpPercentage() {
        const expToNext = expByLevel[this.props.level]
        const percent = expToNext / 100
        return this.props.exp / percent
    }
}

export default PlayerLevelPehavior