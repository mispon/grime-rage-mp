import { CREATE_VEHICLE, LOG_POSITION, GIVE_WEAPON, TELEPORT, TEST } from '../../global/constants/ServerEvents'
import VehiclesManager from '../managers/vehicles/VehiclesManager'
import Misc from './Misc'

const vehicles = ["t20", "zentorno", "osiris", "italigtb2", "cheetah", "xa21", "tyrus", "vacca"]

mp.events.add({
    [CREATE_VEHICLE]: createVehicle,
    [LOG_POSITION]: logPosition,
    [GIVE_WEAPON]: giveWeapon,
    [TELEPORT]: teleport,
    [TEST]: test
})

// Создает транспорт
function createVehicle(player) {
    let position = new mp.Vector3(player.position.x + 2, player.position.y + 2, player.position.z)
    const data = {
        model: vehicles[Misc.randomInt(0, vehicles.length)],
        numberPlate: 'Admin',
        ownerId: player.props.id,
        position: position,
        heading: 0.0,
        fuelTank: 80,
        fuelRate: 4,
        mainColor: Misc.randomInt(0, 159),
        secondColor: Misc.randomInt(0, 159)
    }
    const vehicle = VehiclesManager.createGeneral(data)
    vehicle.isAdmin = true
    player.putIntoVehicle(vehicle, -1)
}

// Логирует позицию
function logPosition(player) {
    const x = formatPosition(player.position.x)
    const y = formatPosition(player.position.y)
    const z = formatPosition(player.position.z)
    const h = formatPosition(player.heading)
    player.outputChatBox(`${x}, ${y}, ${z} | ${h}`)
}

// Выдает оружие
function giveWeapon(player) {
    player.giveWeapon([3220176749, 2210333304], 1000)
}

// Телепортирует игрока
function teleport(player, args) {
    let tp = JSON.parse(args)
    player.position = new mp.Vector3(tp.x, tp.y, tp.z)
}

// Различный экспериментальный код
function test(player) {
    player.spawn(player.position)
}

// Форматирует координату
function formatPosition(value) {
    return value.toFixed(2).toString().replace(/,/g, '.')
}