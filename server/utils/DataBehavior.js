/*
** Расширение для работы с переменными сущности
*/
class DataBehavior {
    constructor(entity) {
        entity.get = this.get
        entity.set = this.set
        entity.reset = this.reset
        entity.has = this.has
    }

    // Возвращает данные
    get(key) {
        return this.getVariable(key)
    }

    // Записывает данные
    set(key, data) {
        this.setVariable(key, data)
    }

    // Очищает данные
    reset(...keys) {
        keys.forEach(key => this.setVariable(key, null))        
    }

    // Проверяет, есть ли данные
    has(key) {
        const data = this.getVariable(key)
        return data != undefined && data != null
    }
}

export default DataBehavior