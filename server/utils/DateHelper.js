import moment from 'moment'

/*
** Вспомогательный класс для работы с датами
*/
class DateHelper {
    constructor() {
        if (!DateHelper.instance) {
            DateHelper.instance = this
        }
        return DateHelper.instance
    }

    // Форматирует дату в презентабельный вид
    format(date) {
        return moment(date).format('YYYY-MM-DD HH:mm')
    }

    // Проверяет, истекло ли время
    timeLeft(date) {
        return date.getTime() <= moment()
    }
}

const instance = new DateHelper()

export default instance