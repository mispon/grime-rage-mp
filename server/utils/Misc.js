/*
** Различные вспомогательные методы
*/
class Misc {
    constructor() {
        if (!Misc.instance) {
            Misc.instance = this
        }
        return Misc.instance
    }

    // Возвращает случайное число в заданном промежутке
    randomInt(min = 0, max = 100) {
        min = Math.ceil(min)
        max = Math.floor(max)
        return Math.floor(Math.random() * (max - min)) + min
    }

    // Возвращает случайное логическое значение
    randomBool() {
        return this.randomInt(0, 2) > 0
    }

    // Возвращает случайную строку указанной длины
    randomString(length, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var result = '';
        for (var i = 0; i < length; i++) {
            var randomPos = Math.floor(Math.random() * charSet.length);
            result += charSet.substring(randomPos, randomPos + 1);
        }
        return result
    }
}

const instance = new Misc()

export default instance