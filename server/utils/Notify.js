import {
    CALL_UI,
    SHOW_ALERT,
    SHOW_HINT,
    HIDE_HINT,
    SHOW_DIALOG 
} from '../../global/constants/ServerEvents'

const DEFAULT_DURATION = 5000

/*
** Вспомогательный класс для отправки уведомлений, алертов и тд.
*/
class Notify {
    constructor() {
        if (!Notify.instance) {
            Notify.instance = this
        }
        return Notify.instance
    }

    // показывает подсказку
    showHint(player, message, target = '', duration = DEFAULT_DURATION) {
        player.call(CALL_UI, [SHOW_HINT, target, message, duration])
    }

    // скрывает подсказку
    hideHint(player) {
        player.call(CALL_UI, [HIDE_HINT])
    }

    // показывает информационное оповещение
    showInfo(player, message, duration = DEFAULT_DURATION) {
        player.call(CALL_UI, [SHOW_ALERT, 'info', message, duration])
    }

    // показывает предупреждающее оповещение
    showWarn(player, message, duration = DEFAULT_DURATION) {
        player.call(CALL_UI, [SHOW_ALERT, 'warning', message, duration])
    }

    // показывает оповещение об успехе
    showSuccess(player, message, duration = DEFAULT_DURATION) {
        player.call(CALL_UI, [SHOW_ALERT, 'success', message, duration])
    }

    // показывает оповещение об ошибке
    showError(player, message, duration = DEFAULT_DURATION) {
        player.call(CALL_UI, [SHOW_ALERT, 'error', message, duration])
    }

    // показывает диалоговое окно
    showDialog(player, title, description = '', onAccept = '', onDecline = '') {
        player.call(SHOW_DIALOG, [title, description, onAccept, onDecline])
    }
}

const instance = new Notify()

export default instance