import { CREATE_NPCS } from '../../global/constants/ServerEvents'
import VectorHelper from './VectorHelper'
import DataBehavior from './DataBehavior'

/*
** Вспомогательный класс для создания маркеров, шейпов, блипов и тд.
*/
class PointCreator {
    constructor() {
        if (!PointCreator.instance) {
            this.npcs = []
            PointCreator.instance = this            
        }
        return PointCreator.instance
    }

    // Создает круппу объектов для нпс
    createNpc(model, position, heading, name = '', dimension = 0) {
        this.npcs.push({model, position, heading, dimension})
        if (name != '') this.createLabel(name, VectorHelper.addZ(position, 1.2), dimension)
        let shape = this.createShape(position, 2.0, dimension)
        return shape
    }

    // создает колшейп
    createShape(position, range = 1, dimension = 0) {
        let shape = mp.colshapes.newCircle(position.x, position.y, range)
        new DataBehavior(shape)
        shape.dimension = dimension
        return shape
    }

    // создает маркер
    createMarker(type, position, color, rotation = new mp.Vector3(), scale = 1, shapeRange = 1, dimension = 0) {
        mp.markers.new(type, position, scale, {
            direction: new mp.Vector3(),
            rotation: rotation,
            color: color,
            visible: true,
            dimension: dimension
        })
        return this.createShape(position, shapeRange)
    }

    // создает текстовую метку
    createLabel(text, position, dimension = 0) {
        mp.labels.new(text, position, {
            los: true,
            font: 4,
            drawDistance: 10,
            dimension: dimension
        })
    }

    // создает блип на карте
    createBlip(position, type, color, name) {
        mp.blips.new(type, position, {
            name: name,
            color: color,
            shortRange: true
        })
    }

    // Отправляет данные об нпс на клиент
    sendNpcsData(player) {
        player.call(CREATE_NPCS, [JSON.stringify(this.npcs)])
    }
}

const instance = new PointCreator()

export default instance