import { ADD_LISTENER, REMOVE_LISTENER } from '../../global/constants/ServerEvents'

mp.events.add({
    [ADD_LISTENER]: addListener,
    [REMOVE_LISTENER]: removeListener
})

// Добавляет нового слушателя
function addListener(player, other) {
    if (other) player.enableVoiceTo(other)
}

// Удаляет слушателя
function removeListener(player, other) {
    if (other) player.disableVoiceTo(other)
}